/**
 * Imports -- modules, packages, assets
 */
import React from "react";
import './modalform.css'


class ModalFormComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        if (!this.props.showing) {
            return (
                <>
                </>
            );
        }

        var smbs = <></>;
        if (this.props.name == "login") {
            smbs = <>
                    <div className="switch-modal-button smb-current">
                        Log In
                    </div>
                    <div className="switch-modal-button smb-not-current" onClick={(e) => this.props.clickHandler(e, "signup")}>
                        Sign Up
                        <div className="smb-border"></div>
                    </div>
                    </>;
        } else if (this.props.name == "signup") {
            smbs = <>
            <div className="switch-modal-button smb-not-current" onClick={(e) => this.props.clickHandler(e, "login")}>
                Log In
                <div className="smb-border"></div>
            </div>
            <div className="switch-modal-button smb-current">
                Sign Up
            </div>
            </>;
        }

        return (
            <div className="modal-form-background" onClick={(e) => this.props.clickHandler(e, "")}>
                <div className="modal-form-content" id="modal-content">

                    {smbs}

                    <form>
                        {this.props.form}
                    </form>
                </div>
            </div>
        );
    }
}

export default ModalFormComponent;