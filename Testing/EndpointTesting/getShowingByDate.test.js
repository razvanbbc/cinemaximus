/********************* Requirements **********************/
const fetch = require("node-fetch");

var movieID = null;
var showingID = [];

beforeAll(async () => {
    var res, json, endpointAddress;

    endpointAddress = "http://localhost:8080/addMovie?movieName=ticketTestMovie&showingIDs=[]&movieAPIID=-1"
    res = await fetch(endpointAddress);
    json = await res.json();

    movieID = json.data.MovieID;

    endpointAddress = "http://localhost:8080/addShowing?movieID=" + movieID + "&screenNumber=-2&startTime=1000";
    res = await fetch(endpointAddress);
    json = await res.json();

    showingID.push(json.data);

    endpointAddress = "http://localhost:8080/addShowing?movieID=" + movieID + "&screenNumber=-2&startTime=1001";
    res = await fetch(endpointAddress);
    json = await res.json();

    showingID.push(json.data);

    endpointAddress = "http://localhost:8080/addShowing?movieID=" + movieID + "&screenNumber=-2&startTime=1002";
    res = await fetch(endpointAddress);
    json = await res.json();

    showingID.push(json.data);
});

//Tests the getMovieTMDB endpoint
it("Getting Showing by Date", async () => {
    var flagCount = 0;
    var keys;

    var endpointAddress = "http://localhost:8080/getShowingsByDate?date=999";
    var res = await fetch(endpointAddress);
    var json = await res.json();

    keys = Object.keys(json.data);

    for (i in showingID) {
        if (keys.includes(showingID[i])) { flagCount = flagCount + 1 }
    }
    expect(flagCount).toEqual(showingID.length);
});