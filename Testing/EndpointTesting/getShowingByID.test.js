/********************* Requirements **********************/
const fetch = require("node-fetch");

var movieID = null;
var showingID = null;

beforeAll(async () => {
    var res, json, endpointAddress;

    endpointAddress = "http://localhost:8080/addMovie?movieName=ticketTestMovie&showingIDs=[]&movieAPIID=-1"
    res = await fetch(endpointAddress);
    json = await res.json();

    movieID = json.data.MovieID;

    endpointAddress = "http://localhost:8080/addShowing?movieID=" + movieID + "&screenNumber=-2&startTime=0";
    res = await fetch(endpointAddress);
    json = await res.json();

    showingID = json.data;
});

//Tests the getMovieTMDB endpoint
it("Getting Showing by ID", async () => {
    var endpointAddress = "http://localhost:8080/getShowingByID?showingID=" + showingID;
    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data.showingID).toEqual(showingID);
});