/**
 * Imports -- modules, packages, assets
 */
 import React from "react"; // should be imported in all components 
 import {
     NavLink,
   } from "react-router-dom";
 
 import "./assets/PaymentForm.css";

 /** HTTP Client */
 import Client from "../../../../HTTP/Client/Client";

 /** User Data */
 import UserProfile from "../../../UserArea/UserProfile";
 
 class CardPaymentFormComponent extends React.Component {
 
     constructor(props) {
         super(props);
         this.state = {
             cardNumber: "",
             expiry: "",
             cvc: "",
         };
 
         this.takeInput = this.takeInput.bind(this);
         this.update = this.update.bind(this);
         this.submitCardDetails = this.submitCardDetails.bind(this);

     }
 
     update(field, event) {
         switch(field) {
             case "cardNumber":
                 var newCardNumber = this.state.cardNumber;
                 if (event.key == "Backspace") {
                     if (newCardNumber.length > 0) {
                         newCardNumber = newCardNumber.substring(0, newCardNumber.length - 1);
                     }
                 } else if (event.keyCode >= 48 && event.keyCode <= 57) {
                     if (newCardNumber.length < 16) {
                         newCardNumber = newCardNumber + event.key;
                     }
                 }
                 event.target.value = newCardNumber.replace(/(.{4})/g, '$1 ').trim();
                 this.setState({cardNumber: newCardNumber});
                 break;
             case "expiry":
                 var newExpiry = this.state.expiry;
                 if (event.key == "Backspace") {
                     if (this.state.expiry.length > 0) {
                         newExpiry = newExpiry.substring(0, newExpiry.length - 1);
                     }
                 } else if (event.keyCode >= 48 && event.keyCode <= 57) {
                     if (newExpiry.length < 4) {
                         newExpiry = newExpiry + event.key;
                     }
                 }
                 event.target.value = newExpiry.replace(/(.{2}?)/g, '$1/').substring(0, 5);
                 this.setState({expiry: newExpiry});
                 break;
             case "cvc":
                 var newCVC = this.state.cvc;
                 if (event.key == "Backspace") {
                     if (newCVC.length > 0) {
                         newCVC = newCVC.substring(0, newCVC.length - 1);
                     }
                 } else if (event.keyCode >= 48 && event.keyCode <= 57) {
                     if (newCVC.length < 3) {
                         newCVC = newCVC + event.key;
                     }
                 }
                 event.target.value = newCVC;
                 this.setState({cvc: newCVC});
                 break;
             default:
                 break;
         }
     }
 
     takeInput(field, event) {
        event.preventDefault();
        this.update(field, event);
     }

    /**
     * Sends the card details found in the state
     * to the backend to be saved
     */
    submitCardDetails(event) {
        event.preventDefault();
        if (!this.state.cardNumber.length || !this.state.cvc.length || !this.state.expiry)  { // one field empty, all fields need to be filled to send data
            console.log("One or more of the required fields are empty. Fill them in and try again.");
            return;
        }
        var data = {
            userID: UserProfile.getData().id,
            cardNo: this.state.cardNumber,
            expiryDate: this.state.expiry,
            CVC: this.state.cvc
        };
        const client = new Client("http://localhost:8080");
        client.requestDataFromEndpoint("/savePaymentData", "post", data, function (response) { // structured as per the Response.js
            if (response.dictionary.message == "Saved card data") {
                window.alert("Successfully saved card details!");
            } else {
                window.alert("Failed to save card details!");
            }
            console.log(response.dictionary);
            return;
        });
    }
 
     render() {
         return (
             <div className="card-payment-form-wrapper-save">
                 <form className="card-payment-form-save">
                     <div className="card-payment-form-line-save">
                         <div className="card-payment-input-wrapper">
                             CARD NUMBER
                             <br/>
                             <input onKeyDown={(e) => this.takeInput("cardNumber", e)} id="card-number-input" className="card-payment-input" type="text" maxLength="16" placeholder="Valid Card Number" autoComplete="off"/>
                         </div>
                     </div>
                     <div className="card-payment-form-line-save">
                         <div className="card-payment-input-wrapper">
                             EXPIRY DATE
                             <br/>
                             <input onKeyDown={(e) => this.takeInput("expiry", e)} id="card-expiry-input" className="card-payment-input" type="text" maxLength="4" placeholder="MM/YY" autoComplete="off"/>
                         </div>
                         <div className="card-payment-input-wrapper">
                             CV CODE
                             <br/>
                             <input onKeyDown={(e) => this.takeInput("cvc", e)} id="card-sc-input" className="card-payment-input" type="text" maxLength="3" placeholder="CVC" autoComplete="off"/>
                         </div>
                     </div>
                     <div className="card-payment-form-line-save">
                         <div id="submit-wrapper" className="card-payment-input-wrapper">
                             <br/>
                             <input id="submit-button" type="submit" value="Save Details" onClick={this.submitCardDetails} />
                         </div>
                     </div>
                 </form>
             </div>
         );
     }
 
 }
 
 export default CardPaymentFormComponent;
 