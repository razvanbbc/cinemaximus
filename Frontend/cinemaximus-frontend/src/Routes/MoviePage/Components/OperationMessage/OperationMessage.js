/**
 * Imports -- modules, packages, assets
 */
 import React from "react";
 import "./OperationMessage.css";
 
 class OperationMessage extends React.Component {
 
     constructor(props) {
         super(props);
     }
 
     render() {
         if (this.props.result !== undefined) {
             if (this.props.result == 1) { // successful operation
                 return (
                     <div className="result success">
                         <h5> Success </h5>
                     </div>
                 );
             }
             else if (this.props.result == 0) { // failed operation
                 return (
                     <div className="result fail">
                         <h5> Fail </h5>
                     </div>
                 );
             }
             else if (this.props.result == -1) { // input fields are empty
                 return (
                     <div className="result fail">
                         <h5> Some required fields for the booking are empty. Review inputs and try again. </h5>
                     </div>
                 );
             }
         }
         else { // operation not executed
             return null;
         }
     }
 
 }
 
 export default OperationMessage;
 
