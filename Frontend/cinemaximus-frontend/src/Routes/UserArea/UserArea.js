/**
 * Imports -- modules, packages, assets
 */
import React, { Component } from "reactn"; // should be imported in all components

import './assets/UserArea.css'
import ButtonComponent from "./Components/Button/Button";
import SearchBarComponent from "./Components/SearchBar/SearchBar";
import TicketComponent from "./Components/TicketView/TicketView";
import ModalFormComponent from "./Components/ModalForm/ModalForm";

import Authentification from "./Authentification"
import firebase from "../../Firebase";
import UserProfile from "./UserProfile";

import { resolve } from "path";
import { useParams } from "react-router-dom";

class UserArea extends React.Component {

    constructor(props) {
        super(props);

        this.switchModal = this.switchModal.bind(this);
        this.getForm = this.getForm.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.setUser = this.setUser.bind(this);
        this.logout = this.logout.bind(this);

        this.state = {
            activeModal: "",
            formData: {},
        };

        if (props.location.state != undefined) {
            this.state.activeModal = props.location.state.action;
        }

    }

    // name options: "login", "signup", "" (empty string for none)
    switchModal(event, name) {
        if (name == "" && event.target.className != "modal-form-background") { // haven't actual clicked off it, shouldn't close
            return;
        }
        this.setState({
            activeModal: name,
        });
    }

    // takes data from fb response (i.e. response[0]) and uses relevant info to set user
    setUser(response) {
        var data = {
            id: response.uid,
            name: response.displayName,
        };
        UserProfile.setData(data);
        this.forceUpdate();
    }

    // gets data from form and puts it in a dictionary
    submitForm() {
        var fields = document.getElementsByClassName("modal-form-field");
        var data = {};
        for (var field of fields) {
            data[field.id] = field.value;
        }

        var auth = new Authentification(firebase);
        if (this.state.activeModal == "login") {
            auth.login(data["email"], data["password"], function(response) {
                if (response[0] != undefined) {
                    // success
                    console.log("successfully logged in");
                    this.setState({
                        activeModal: "",
                    });
                    this.setUser(response[0]);
                    window.location.reload();
                } else if (response[1] != undefined) {
                    console.log(response);
                    // failure
                }
            }.bind(this));
        } else if (this.state.activeModal == "signup") {
            auth.signUp(data["name"], data["email"], data["password"], function(response) {
                if (response[0] != undefined) {
                    // success
                    console.log("successfully signed up");
                    this.setState({
                        activeModal: "",
                    });
                    this.setUser(response[0]);
                    window.location.reload();
                } else if (response[1] != undefined) {
                    // failure
                }
            }.bind(this));
        }
    }

    // names: login, signup
    getForm(name) {
        switch(name) {
            case "login":
                return [
                    <input key="0" id="email" className="modal-form-field modal-form-text-input" type="text" placeholder="Email" autoComplete="off" />,
                    <input key="1" id="password"  className="modal-form-field modal-form-text-input" type="password" placeholder="Password" autoComplete="off" />,
                    <input key="2" id="submit"  className="modal-form-submit" type="button" value="Log In" onClick={this.submitForm} />,
                ];
            case "signup":
                return [
                    <input key="0" id="name" className="modal-form-field modal-form-text-input" type="text" placeholder="Full Name" autoComplete="off" />,
                    <input key="1" id="email" className="modal-form-field modal-form-text-input" type="text" placeholder="Email" autoComplete="off" />,
                    <input key="2" id="password"  className="modal-form-field modal-form-text-input" type="password" placeholder="Password" autoComplete="off" />,
                    <input key="3" id="confirm-password"  className="modal-form-field modal-form-text-input" type="password" placeholder="Confirm Password" autoComplete="off" />,
                    <input key="4" id="submit"  className="modal-form-submit" type="button" value="Sign up" onClick={this.submitForm} />,
                ];
            default: return [];
        }
    }

    logout() {
        var auth = new Authentification(firebase);
        auth.logout(function(response) {
            console.log(response);
            if (response == "SUCCESS") {
                UserProfile.setData({
                    id: "",
                    name: "",
                });
                console.log("Successfully logged out!");
                window.location.reload();
                this.forceUpdate();
            } else {
                console.error("Failed to log out!")
            }
        }.bind(this));
    }

    render() {

        var userData = UserProfile.getData();
        if (userData.id != "") {
            // logged in
            var greeting = "";
            if (userData.id == "0jnIWg2Fl8N44zDT5sZXM1En2S23") { // manager logged in (no display name)
                greeting = "Hello, boss !";
            }
            else if (userData.name != null && userData.name != "" && userData.name != undefined) { // covering my bases
                greeting = "Hi, " + userData.name + "!";
            }
            return (
                <div className="center-all">
                    <h1 className="greeting"> {greeting} </h1>
                    <div className="accountButtonsWrapper">
                        <ButtonComponent name="logout" class="accountButton" label="Log Out" clickHandler={this.logout} />
                    </div>
                    <div className="ticketFinderWrapper">
                        <h1>Find a Ticket</h1>
                        <h5 id="enter-ticket-span">(Enter Ticket ID)</h5>
                        <SearchBarComponent />
                        <hr/>
                        <TicketComponent data={this.global.currentViewedTicket}/>
                    </div>
                </div>
            );
        }

        return (
            <div className="center-all">
                <ModalFormComponent name="login" form={this.getForm("login")} showing={this.state.activeModal == "login"} clickHandler={this.switchModal} />
                <ModalFormComponent name="signup" form={this.getForm("signup")} showing={this.state.activeModal == "signup"} clickHandler={this.switchModal} />
                <div className="accountButtonsWrapper">
                    <ButtonComponent name="login" class="accountButton" label="Log In" clickHandler={this.switchModal} />
                    <ButtonComponent name="signup" class="accountButton" label="Sign Up" clickHandler={this.switchModal} />
                </div>
                <div className="ticketFinderWrapper">
                    <h1>Find a Ticket</h1>
                    <h5 id="enter-ticket-span">(Enter Ticket ID)</h5>
                    <SearchBarComponent />
                    <hr/>
                    <TicketComponent data={this.global.currentViewedTicket}/>
                </div>
            </div>
        );
    }

}

export default UserArea;
