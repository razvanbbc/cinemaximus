/**
 * Imports -- modules, packages, assets
 */
 import React from "react";

 import './assets/ManagerOperationsPage.css';
 import '../UserArea/UserProfile';
 import UserProfile from "../UserArea/UserProfile";

 /** HTTP Client Logic */
 import Client from "../../HTTP/Client/Client";

 /**
  * UI - Manager Operations (Analytics)
  */
 class ManagerOperationsPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            TicketsSoldPerMovie: [],
            TicketsIncome: [],
            isManager: false,
            barOptions: {},
            lineOptions: {},
            completedProcessingIncome: false 
        }

        this.userIsManager = this.userIsManager.bind(this);
        this.loadingRender = this.loadingRender.bind(this);

        if (!this.userIsManager()) {
            window.location.href = "/";
        }

    }

    userIsManager() {
        try {
            var id = UserProfile.getData().id;
            if (id == "0jnIWg2Fl8N44zDT5sZXM1En2S23") {
                this.state.isManager = true;
                return true;
            } else {
                return false;
            }
        } catch (error) {
            return false;
        }
        return false;
    }

    componentDidMount() {

        const client = new Client("http://localhost:8080");
        var _this = this; // strong reference to this so it doesn't get lost in callbacks

        // Get ticket count data
        client.requestDataFromEndpoint("/calcTicketCount", "get", {startDate: 1615127950, endDate: 1617031501}, function (response) { // structured as per the Response.js
          // console.log(response)
          var entries = [];
          for (var key in response.dictionary.data) {
            const value = response.dictionary.data[key];
            const entry = {y: value, label: key};
            entries.push(entry);


            // configures the bar graph data and interface
            const barOptions = {
                animationEnabled: true,
                theme: "light2",
                title: {
                    text: "Tickets Sold per Movie"
                },
                axisX: {
                    title: "Movie",
				    reversed: true,
                },
                axisY: {
                    title: "Sold Tickets",
				    includeZero: true,
                },
                data: [
                    {
                        type: "bar",
                        dataPoints: entries
                    }
                ]
            }
            _this.setState({barOptions: barOptions});
          }
        });
        
        // Get ticket income data
        // Date.now()
        client.requestDataFromEndpoint("/calcTicketIncome", "get", {startDate: 1615000000, endDate: 1617000000}, function (response) { // structured as per the Response.js
            // console.log(response.data);
            var entries = [];
            for (var key in response.data[1]) {
 
                var formattedUnixDate = new Date(key * 1000).toLocaleDateString("en-GB");
                const entry = {y: response.data[1][key], label: formattedUnixDate};
            
                entries.push(entry);

                // configures the bar graph data and interface
                const lineOptions = {
                    animationEnabled: true,
                    title: {
                        text: "Weekly Sales - Past Year"
                    },
                    axisY: {
                        title: "Sales (in GBP)",
                        prefix: "£"
                    },
                    data: [
                        {
                            yValueFormatString: "£#,###",
                            xValueFormatString: "MMMM",
                            type: "spline",
                            dataPoints: entries
                        }
                    ]
                }
                _this.setState({lineOptions: lineOptions});

            }

        });
        
    }

    /**
     * Returns a data dictionary example for the bar chart
     */
    getExampleBarChartData() {
        return [
            {y: 500, label: "Avatar"},
            {y: 250, label: "Space Sweepers"},
            {y: 999, label: "Soul"},
        ];
    }

    /**
     * Returns a data dictionary example for the spline chart
     */
    getExampleSplineChartData() {
        return [
            {x: new Date(2017, 0, 1), y: 25060},
            {x: new Date(2017, 0, 2), y: 27980},
            {x: new Date(2017, 0, 3), y: 28000}
        ];
    }

    loadingRender() {
        if (this.state.completedProcessingIncome == false) {
            return (
                <div className="loading-wrapper center-all">
                    <h3> Calculating income ... </h3>
                </div>
            );
        }
        else {
            return null;
        }
    }

    render() {

        if (this.state.isManager == false) {
            return <></>;
        }

        // third party lib for graphs
        var CanvasJSReact = require("../../Libs/canvasjs-non-commercial-3.2.11/canvasjs.react");
        var CanvasJSChart = CanvasJSReact.default.CanvasJSChart;

        const lineOptions = {
            animationEnabled: true,
			title: {
				text: "Weekly Sales - Past Year"
			},
			axisX: {
				valueFormatString: "MMM"
			},
			axisY: {
				title: "Sales (in GBP)",
				prefix: "£"
			},
			data: [
                {
				    yValueFormatString: "£#,###",
				    xValueFormatString: "MMMM",
				    type: "spline",
				    dataPoints: this.getExampleSplineChartData()
                }
            ]
        }

        return (
            <div className="wrapper">
                <div className="header">
                    <h3> Business Analytics </h3>
                    <hr/> 
                </div>
                <div className="stats-wrapper">
                    <div className="sold-bar-chart-wrapper">
                        <div className="sold-bar-chart">
                            <CanvasJSChart className="chart" options={this.state.barOptions} />
                        </div>
                    </div>
                    <br/><br/><br/><br/><br/>
                    <hr/>
                    <br/><br/>
                    <div className="income-line-chart-wrapper">
                        {this.loadingRender()}
                        <div className="income-line-chart-additional-container">
                            <div className="income-line-chart">
                                <CanvasJSChart options={this.state.lineOptions} />
                            </div>   
                        </div>                     
                    </div>
                </div>
            </div>
        );
    }

 }

 export default ManagerOperationsPage;