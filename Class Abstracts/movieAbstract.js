/*
 * abstract for the movie class implementations in backend and frontend
*/

/*
 * {
 *      int: movieID            (primary key)
 *      string: movieName       (name of the movie)
 *      *int[]: showingIDs      (list of showings)
 *      string: movieAPIID      (primary key of movie in IMDb)
 * }
*/

class movieAbstract
{
    constructor(movieID, movieName)
    {
        if (this.constructor == movieAbstract)
        {
            throw new Error("Cannot instanciate abstract class");
        }

        this.movieID = movieID;
        this.movieName = movieName;
        this.showingIDs = [];
        this.movieAPIID = -1;
    }

    /** Getters */
    get dictionary() // returns the format below
    { 
        return {
            "movieID": this.movieID,
            "movieName": this.movieName,
            "showingIDs": this.showingIDs,
            "movieAPIID": this.movieAPIID
        };
    }
}

module.exports = movieAbstract;