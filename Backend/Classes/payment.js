/*
 * concrete implementation of the payment class for backend
*/

/*
 * {
 *      string: paymentID          (primary key)
 *      string: cardName        (name on the car)
 *      int: cardNo             (the card number)
 *      int: securityDigits     (security digits required to authorise a purchase)
 * }
*/

const Abstract = require("../../Class Abstracts/paymentAbstract");

class Payment extends Abstract {
    constructor(cardName, cardNo, securityDigits) {
        var holdCNa, holdCNo, holdSD;

        if (typeof cardName == "string") { holdCNa = cardName }
        else { holdCNa = "Unkown" }

        if (typeof cardNo == "string") { holdCNo = cardNo }
        else { holdCNo = "-1" }

        if (Number.isInteger(securityDigits) == true) { holdSD = securityDigits }
        else { holdSD = -1 }

        super(0, holdCNa, holdCNo, holdSD);
    }

    setPaymentID(paymentID) {
        if (typeof paymentID == "string") { this.paymentID = paymentID }
        else { console.log("paymentID must be a string in setPaymentID(paymentID)") }
    }

    setCardName(cardName) {
        if (typeof cardName == "string") { this.cardName = cardName }
        else { console.log("cardName must be a string in setCardName(cardName)") }
    }

    setCardNo(cardNo) {
        if (Number.isInteger(cardNo) == true) { this.cardNo = cardNo }
        else { console.log("cardNo must be a integer in setCardNo(cardNo)") }
    }

    setSecurityDigits(securityDigits) {
        if (Number.isInteger(securityDigits) == true) { this.securityDigits = securityDigits }
        else { console.log("securityDigits must be an integer in setSecurityDigits(securityDigits)") }
    }

    getPaymentID() { return this.paymentID }

    getCardName() { return this.cardName }

    getCardNo() { return this.cardNo }

    getSecurityDigits() { return this.securityDigits }
}

module.exports = Payment;