/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink, Link,
  } from "react-router-dom";

import './filmscreenings.css'
import ScreeningButtonComponent from "../ScreeningButton/ScreeningButton";
import Client from "../../../../../../../../HTTP/Client/Client";

class FilmScreeningsComponent extends React.Component {

    constructor(props) {
        super(props);
        this.makeShowingButtons = this.makeShowingButtons.bind(this);
        this.setStorage = this.setStorage.bind(this);
    }

    componentDidMount() {
        this.makeShowingButtons();
    }

    makeShowingButtons() {
        var result = [];
        for (var i = 0; i < this.props.times.length; i++) {
            result.push(
                <ScreeningButtonComponent key={i} id={this.props.times[i]["showingID"]} timestamp={this.props.times[i]["startTime"]} clickHandler={this.setStorage}/>
            );
        }
        return result;
    } 
    
    setStorage() {
        
        var allMovies = JSON.parse(localStorage.getItem("allMovies"));
        let details = {};
        for (var deet of allMovies) {
            if (deet["id"] == this.props.id) {
                details = deet;
                break;
            }
        }
        let savedState =  JSON.stringify(details);
        localStorage.setItem("currentMovieState", savedState);
    }

    render() {
        var sb = this.makeShowingButtons();
        return (
            <div className="filmScreeningsWrapper">
                <div className="filmScreeningName">
                    <Link className="movieLink" to={{
                    pathname: "/movie",
                    state: {},
                    }} onClick={() => {
                            this.setStorage();
                    }}>
                    {this.props.name}
                    </Link>
                </div>
                <div className="filmScreeningButtons">
                    {sb}
                </div>
            </div>
        );
    }

}

export default FilmScreeningsComponent;
