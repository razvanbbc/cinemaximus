/********************* Requirements **********************/
const fetch = require("node-fetch");

var movieID = null;

beforeAll(async () => {
    var res, json, endpointAddress;

    endpointAddress = "http://localhost:8080/addMovie?movieName=jestMovie&showingIDs=[]&movieAPIID=-1"
    res = await fetch(endpointAddress);
    json = await res.json();

    movieID = json.data.MovieID;
});

//Tests the getMovieTMDB endpoint
it("Adding a showing", async () => {
    var endpointAddress = "http://localhost:8080/addShowing?movieID=" + movieID + "&screenNumber=-2&startTime=0";
    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data.length).toEqual(20);
});