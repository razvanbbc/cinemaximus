/**
 * Imports -- modules, packages, assets
 */
import React, { setGlobal } from "reactn"; // should be imported in all components 
import { 
  Route, 
  HashRouter } from "react-router-dom"; // navbar logic
import './App.css';
import NavbarComponent from "./Components/Navigation/Navbar";
import FooterComponent from "./Components/Footer/Footer";

import Home from "./Routes/Home/Home";
import WhatsOn from "./Routes/WhatsOn/WhatsOn";
import UserArea from "./Routes/UserArea/UserArea";
import MoviePage from "./Routes/MoviePage/MoviePage";
import ManagerOperationsPage from "./Routes/Managing/ManagerOperationsPage";
import PaymentDetailsPage from "./Routes/PaymentDetails/PaymentDetailsPage";

/** HTTP Client Logic */
import Client from "./HTTP/Client/Client";

class App extends React.Component { // component architecture

  // Component constructor
  constructor(props) {
    super(props);
    // other state variables set here
  }

  /**
   * Will get called before rendering
   *
   * This is an example of the HTTP client use case
   */
  componentDidMount() {
    const client = new Client("http://localhost:8080");
    client.requestDataFromEndpoint("/", "get", {}, function (response) { // structured as per the Response.js
      // console.log(response)
      console.log(response.dictionary);
    });
  }

  isLoggedIn() {
    if (localStorage.getItem("user_info") === undefined || localStorage.getItem("user_info").id == "") {
      return false;
    }
    else if (localStorage.getItem("user_info") !== undefined && localStorage.getItem("user_info").id != "") {
      return true;
    }
  }

  /*
   * Render component
   *
   * App.js holds an abstract structure, navbar and footer
   * and the content is changed according to the selected route
  */
  render() {
    return (
        <HashRouter>
          <div className="wrapper">
            <NavbarComponent />
            <div className="content">
              <Route exact path="/" component={Home} />
              <Route path="/home" component={Home} />
              <Route path="/whatson" component={WhatsOn} />
              <Route path="/userarea" render={(props) => <UserArea {...props} key={Math.random()}/>} />
              {/** Routes from non-navbar operations */}
              <Route path="/movie" component={MoviePage} />
              <Route path="/manager" component={ManagerOperationsPage} />
              <Route path="/paymentDetails" component={PaymentDetailsPage} />
            </div>
            <br />
            <br />
            <FooterComponent />
          </div>
        </HashRouter>
    );
  }

}

export default App;
