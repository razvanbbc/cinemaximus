/********************* Requirements **********************/
const fetch = require("node-fetch");

/********************* dotenv Init **********************/
require('dotenv').config(); // makes .env files available for use in the project (useful to store API keys, config vars)

/********************* Firebase Config **********************/
var firebase = require('firebase');

const firebaseConfig = { // populated from .env
    apiKey: process.env.apiKey,
    authDomain: process.env.authDomain,
    databaseURL: process.env.databaseURL,
    projectId: process.env.projectId,
    storageBucket: process.env.storageBucket,
    messagingSenderId: process.env.messagingSenderId,
    appId: process.env.appId,
    measurementId: process.env.measurementId
};
try {
    firebase.initializeApp(firebaseConfig);
}
catch (e) {
    console.error(e);
}


//Tests the getMovieTMDB endpoint
test("Adding a Movie", async () => {
    var endpointAddress = "http://localhost:8080/addMovie?movieName=jestMovie&showingIDs=[9,8,7]&movieAPIID=-1"

    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data.MovieID.length).toEqual(20);
});