import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import WhatsOn from "./WhatsOn";
import FilmLinksAreaComponent from "./Components/FilmLinksArea/FilmLinksArea";

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("WhatsOn renders", () => {
    act(() => {
        render(<WhatsOn />, container);
    });
});