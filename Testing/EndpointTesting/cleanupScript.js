/********************* dotenv Init **********************/
require('dotenv').config(); // makes .env files available for use in the project (useful to store API keys, config vars)

/********************* Server Listener **********************/

const express = require('express');
const port = 8082; // read from .env using process.env.<var name>
const app = express();

/********************* Firebase Config **********************/
var firebase = require('firebase');

const firebaseConfig = { // populated from .env
    apiKey: process.env.apiKey,
    authDomain: process.env.authDomain,
    databaseURL: process.env.databaseURL,
    projectId: process.env.projectId,
    storageBucket: process.env.storageBucket,
    messagingSenderId: process.env.messagingSenderId,
    appId: process.env.appId,
    measurementId: process.env.measurementId
};
try {
    firebase.initializeApp(firebaseConfig);
}
catch (e) {
    console.error(e);
}

/********************* Data to Clean **********************/
//movie names to remove
const movies = ["jestMovie", "movieTestMovie", "ticketTestMovie", "ticketCountMovie"];
//screen number of showings to remove
const showings = [-2];
//buyers names of tickets to remove
const tickets = ["test man", "testBuyer", "calcCountTicket"];

app.get('/', (req, res) => {
    console.log("Cleaning DB");
    cleanDBMovies()
    cleanDBShowings();
    cleanDBTickets();
    console.log("Cleaned");
    console.log(" ");
});

//cleans the Movies table of the test instances created
function cleanDBMovies() {
    var ref = firebase.database().ref("Movies")

    console.log("   Cleaning Movies Table");
    for (i in movies) {
        cleanByMovieName(ref, movies[i]);
	}
}

function cleanByMovieName(ref, movieName) {
    ref.orderByChild('movieName').equalTo(movieName).get().then(function (snap) {
        var data = snap.val();
        for (ID in data) {
            firebase.database().ref('Movies/' + ID).remove();
		}
    });
}

/*
function cleanDBPayments(testPayment) {
    var ref = firebase.database().ref("Payments");
    ref.orderByChild('cardName').equalTo(testPayment.getCardName()).on('value', (snap) => {
        ref.off('value', undefined, this);
        snap.forEach((childSnap) => {
            firebase.database().ref('Payments/' + childSnap.key).remove();
        });
    });
}
*/

//cleans the Showings table of the test instances created
function cleanDBShowings() {
    var ref = firebase.database().ref("Showings");
    console.log("   Cleaning Showings Table");
    for (i in showings) {
        cleanShowingsByScreenNo(ref, showings[i]);
	}
}

function cleanShowingsByScreenNo(ref, screenNo) {
    ref.orderByChild('screenNumber').equalTo(screenNo).get().then(function (snap) {
        var data = snap.val();
        for (ID in data) {
            firebase.database().ref('Showings/' + ID).remove();
		}
    });
}

function cleanDBTickets(testTicket) {
    var ref = firebase.database().ref("Tickets");
    console.log("   Cleaning Tickets Table");
    for (i in tickets) {
        cleanTicketsByName(ref, tickets[i]);
	}
}

function cleanTicketsByName(ref, buyersName) {
    ref.orderByChild('buyersName').equalTo(buyersName).get().then(function (snap) {
        var data = snap.val();
        for (ID in data) {
            firebase.database().ref('Tickets/' + ID).remove();
		}
    });
}

/*
function cleanDBUsers(testUser) {
    var ref = firebase.database().ref("Users");
    ref.orderByChild('name').equalTo(testUser.getName()).on('value', (snap) => {
        ref.off('value', undefined, this);
        snap.forEach((childSnap) => {
            firebase.database().ref('Users/' + childSnap.key).remove();
        });
    });
}
*/

// Listen
app.listen(port, () => console.log(`Server listening on ${port} ...`));
