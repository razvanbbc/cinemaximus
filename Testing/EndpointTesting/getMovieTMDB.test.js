/********************* Requirements **********************/
const fetch = require("node-fetch");

//Tests the getMovieTMDB endpoint
it("Fetching TMDB Data", async () => {
    var endpointAddress = "http://localhost:8080/getMovieTMDB?filmAPIID=581389";
    var targetName = "Space Sweepers";
    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data.Title).toEqual(targetName);
});