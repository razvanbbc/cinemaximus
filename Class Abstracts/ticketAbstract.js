/*
 * abstract for the ticket class implementations in backend and frontend
*/

/*
 * {
 *      int: ticketID           (primary key)
 *      *int: movieID           (ID of the movie the ticket is for)
 *      *int: showingID         (number of the screen which the ticket is for)
 *      string: seatID          (ID of the seat should contain two integers seperated by a ':')
 *      string: buyersName      (name of the buyer of the ticket)
 *      string: email           (email the ticket is sent to)
 *      float: price              (the price of the ticket in pounds)
 * }
*/

class ticketAbstract
{
    constructor(ticketID, movieID, showingID, seatID, buyersName, email, price)
    {
        if (this.constructor == ticketAbstract)
        {
            throw new Error("Cannot instanciate abstract class");
        }

        this.ticketID = ticketID;
        this.movieID = movieID;
        this.showingID = showingID;
        this.seatID = seatID;
        this.buyersName = buyersName;
        this.email = email;
        this.price = price;
    }

    /** Getters */
    get dictionary() // returns the format above
    {
        return {
            "ticketID": this.ticketID,
            "movieID": this.movieID,
            "showingID": this.showingID,
            "seatID": this.seatID,
            "buyersName": this.buyersName,
            "email": this.email,
            "price": this.price
        };
    }
}

module.exports = ticketAbstract;
