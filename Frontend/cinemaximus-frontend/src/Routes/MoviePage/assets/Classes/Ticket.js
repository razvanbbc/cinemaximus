/**
 * Ticket logical structure for the frontend
 */
class Ticket {

    constructor(movieID, showingID, seatID, buyersName, email) {

        this.movieID = movieID;
        this.showingID = showingID;
        this.seatID = seatID;
        this.buyersName = buyersName;
        this.email = email;

    }

    /** Getters */
    get dictionary() { // returns the format above 
        return {
            "movieID": this.movieID,
            "showingID": this.showingID,
            "seatID": this.seatID,
            "buyersName": this.buyersName,
            "email": this.email
        };
    }
}

export default Ticket;