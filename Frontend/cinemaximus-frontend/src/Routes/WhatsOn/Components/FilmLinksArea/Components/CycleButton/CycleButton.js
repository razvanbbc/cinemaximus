/**
 * Imports -- modules, packages, assets
 */
import React from "react";
import {
    Link,
  } from "react-router-dom";

import './cyclebutton.css';

class CycleButtonComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="cycle-button" onClick={this.props.handleClick}>
            {this.props.symbol}
            </div>
        );
    }
}

export default CycleButtonComponent;