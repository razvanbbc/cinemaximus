/*
 * Entry point for the NodeJS and Express application
 * This script runs when command 'node index.js' is executed in the Backend/ folder
 */

/********************* dotenv Init **********************/
require('dotenv').config(); // makes .env files available for use in the project (useful to store API keys, config vars)

/********************* NPM Requires **********************/
const express = require('express');
const cors = require('cors'); // middle package for express
const bodyParser = require('body-parser'); // for URL parameter querying
const fetch = require("node-fetch");
const url = require('url');
const QRCode = require('qrcode');

var firebase = require('firebase');

/********************* Implementation Requires **********************/
const ResponseClass = require('./Classes/Response/response');

const HandleMovies = require('./Classes/Firebase/MovieSys/handleMovie');
const HandlePayments = require('./Classes/Firebase/PaymentSys/handlePayment');
const HandleShowings = require('./Classes/Firebase/ShowingSys/handleShowing');
const HandleUsers = require('./Classes/Firebase/UserSys/handleUser');
const HandleTicket = require('./Classes/Firebase/TicketSys/handleTicket');
const handleManagement = require('./Classes/Managing/handleManagement')

const TMDB = require('./TMDB');
const { response } = require('express');

const EmailerImport = require('./Classes/Emailer/emailer');

 /********************* Firebase Config **********************/
 const firebaseConfig = { // populated from .env
   apiKey: process.env.apiKey,
   authDomain: process.env.authDomain,
   databaseURL: process.env.databaseURL,
   projectId: process.env.projectId,
   storageBucket: process.env.storageBucket,
   messagingSenderId: process.env.messagingSenderId,
   appId: process.env.appId,
   measurementId: process.env.measurementId
};
try {
    firebase.initializeApp(firebaseConfig);
}
catch(e) {
    console.error(e);
}

 /********************* TMDB Config **********************/
 const TMDBapiKey = process.env.TMDBapiKey;

 /********************* Setting up DB Handlers **********************/
const movieHandler = new HandleMovies(firebase);
const paymentHandler = new HandlePayments(firebase);
const showingHandler = new HandleShowings(firebase);
const userHandler = new HandleUsers(firebase);
const ticketHandler = new HandleTicket(firebase);
const managementHandler = new handleManagement(firebase);

/********************** Setting up classes  **********************/
const movieClass = require('./Classes/movie');
const paymentClass = require('./Classes/payment');
const showingClass = require('./Classes/showing');
const ticketClass = require('./Classes/ticket');
const userClass = require('./Classes/user');

/********************* Server Listener **********************/
const port = process.env.APP_PORT; // read from .env using process.env.<var name>
const app = express();

// Cors and body-parser
app.use(cors());
app.use(express.urlencoded({
    extended: true
})); // parse URL-encoded bodies (as sent by HTML forms)
app.use(express.json()); // parse JSON bodies (as sent by API clients)

// CRUD Methods (req = url parameter querying, res = response sender)
app.get('/', (req, res) => { // localhost:8080/

    // Response builder
    const response = new ResponseClass();
    response.setStatus(200); // common status codes found online
    response.setSuccessfulOperation(true); // operation processed successfully
    response.setOperations(0); // basic operation 0, test if listener works
    response.setMessage(`Server is listening on port ${port}`);
    response.setData({
        "Hello": "World"
    });

    // Send response using the res parameter
    res.json(response.dictionary);
});

//on .../getMovieTMDB?filmAPIID=<<TMDBID>>
//returns the API address of the movie and a json file of importants data for a given movie.
//returns {'Title': <<title>>, 'Overview': <<overview>>, 'Poster Path': <<path>>, 'Release Date': <<release date>>, 'Genres': <<genres>>, 'Languages': <<spoken languages>>, 'Adult': <<adult>>}
app.get('/getMovieTMDB', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;
    var response;

    TMDB.fetchByID(TMDBapiKey, qData.filmAPIID, function (data) {
        try {
            response = new ResponseClass(200, true, 0, "Found movie details", data);
        } catch (e) {
            console.error(e);
            response = new ResponseClass(400, false, 0, "Failed to find movie details", null);
        }
        finally { res.json(response.dictionary); }
    });
});

//on .../addMovie?movieName=<<name>>&showingIDs=<<id1>>,<<id2>>,<<id3>>,...&movieAPIID=<<movie API ID>>
//adds a movie with the values passed to the movie table
//returns {'MovieID': <<movieID>>}
app.get('/addMovie', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;
    var response, movieID = null;

    movieHandler.addMovie(qData.movieName, qData.showingIDs, qData.movieAPIID, function (data) {
        movieID = data;
        if (typeof movieID != "string" || movieID == "-1") { response = new ResponseClass(400, false, 1, "Failed to add movie", null); }
        else { response = new ResponseClass(200, true, 1, "Added movie", { 'MovieID': movieID }); }
        res.json(response.dictionary);
    });
});

//on .../getMovieByID?movieID=<<ID>>
//gets a movie from the database with the ID passed
//returns {'ID': <<ID>>, 'Name': <<name>>, 'Showings': [id1, id2, ...], 'APIID': <<API ID>>, 'Details': {TMDB data}}
app.get('/getMovieByID', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;
    var response, thisMovie;
    movieHandler.getMovieByID(qData.movieID, function (data) {
        thisMovie = data;
        if (thisMovie.getMovieID() != -1 && thisMovie.getMovieAPIID() != -1) {
            TMDB.fetchByID(TMDBapiKey, thisMovie.getMovieAPIID(), function (data) {
                response = new ResponseClass(200, true, 3, "Found Movie", data)
                res.json(response.dictionary);
            }, function (e) {
                console.error(e);
                response = new ResponseClass(400, false, 3, "Failed to find movie details", null);
                res.json(response.dictionary);
            });
        }
        else if (thisMovie.getMovieID() != -1 && thisMovie.getMovieAPIID() == -1) {
            response = new ResponseClass(200, true, 3, "Found Movie, no TMDB ID", data)
            res.json(response.dictionary);
        }
        else {
            response = new ResponseClass(400, false, 3, "Failed to find movie details", null);
            res.json(response.dictionary);
        }
    }, function (e) {
            console.error(e);
            response = new ResponseClass(400, false, 3, "Failed to find Movie", thisMovie);
            res.json(response.dictionary);
    });
});

//on .../getMovieByName?movieName=<<name>>
//gets a movie from the database with the name passed
//returns {'ID': <<ID>>, 'Name': <<name>>, 'Showings': [id1, id2, ...], 'APIID': <<API ID>>, 'Details': {TMDB data}}
app.get('/getMovieByName', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;
    var response, thisMovie;

    movieHandler.getMovieByName(qData.movieName, function (data) {
        thisMovie = data;
        if (thisMovie.getMovieID() != -1 && thisMovie.getMovieAPIID() != -1) {
            TMDB.fetchByID(TMDBapiKey, thisMovie.getMovieAPIID(), function (data) {
                thisMovie = { thisMovie, 'Details': data };
                response = new ResponseClass(200, true, 2, "Found Movie", thisMovie)
                res.json(response.dictionary);
            }, function (e) {
                console.error(e);
                response = new ResponseClass(400, false, 2, "Failed to find movie details", null);
                res.json(response.dictionary);
            });
        }
        else if (thisMovie.getMovieID() != -1 && thisMovie.getMovieAPIID() == -1) {
            response = new ResponseClass(200, true, 3, "Found Movie, no TMDB ID", data)
            res.json(response.dictionary);
        }
        else {
            response = new ResponseClass(400, false, 3, "Failed to find movie details", null);
            res.json(response.dictionary);
        }
    }, function (e) {
            console.error(e);
        response = new ResponseClass(400, false, 2, "Failed to find Movie", thisMovie);
        res.json(response.dictionary);
    });
});

//on .../getAllMovies
//gets all movies in the database
//returns {{<<movie1>>}, {<<movie2>>}, ..., {movieN}}
app.get('/getAllMovies', (req, res) => {
    var response;
    var movies = [];

    movies = movieHandler.getAllMovies(function (data) {
        movies = data;
        response = new ResponseClass(200, true, 8, "Returned movies", movies);
        res.json(response.dictionary);
    }, function (e) {
            console.error(e);
            response = new ResponseClass(400, false, 8, "Failed to return Movies", movies);
            res.json(response.dictionary);
    });
})

//on .../getMoviesByKeywords?keywords=<<keyword1>>,<<keyword2>>, ...
//gets all movies whose names contain all keywords
//returns {{<<movie1>>}, {<<movie2>>}, ..., {movieN}}
app.get('/getMoviesByKeywords', (req, res) => {
    const kwString = req.query.keywords;
    const kwArray = kwString.split(",");
    var movies = [];
    var flag = false, thisName;

    try {
        movieHandler.getAllMovies(function (data) {
            for (ID in data) {
                flag = false;
                thisName = data[ID].movieName;
                for (i in kwArray) {
                    var nameArray = thisName.split(" ");
                    var holdName = "";

                    for (x in nameArray) { holdName = holdName + nameArray[x].charAt(0).toUpperCase() + nameArray[x].slice(1).toLowerCase() + " "; }
                    holdName = holdName.trim();

                    if (thisName.includes(kwArray[i]) == true || thisName.toLowerCase().includes(kwArray[i]) == true || thisName.toUpperCase().includes(kwArray[i]) == true || holdName.includes(kwArray[i] == true)) {
                        flag = true;
                    }
                }
                if (flag == true) {
                    var holdMovie = new movieClass(data[ID].movieName, [], data[ID].movieAPIID);
                    holdMovie.setShowingIDs(data[ID].showingIDs);
                    holdMovie.setMovieID(ID);
                    movies.push(holdMovie);
                }
            }
            var response = null;
            if (movies.length > 0) { response = new ResponseClass(200, true, 11, "Returning matching movies", movies); }
            else { response = new ResponseClass(400, false, 11, "Failed to find any matching movies", []); }
            res.json(response.dictionary);
        });
    } catch {
        var response = new ResponseClass(400, false, 11, "Crashed for unknown reason", null);
        res.json(response.dictionary);
    }
});

//on .../storeTicket?movieID=<<ID>>&showingID=<<ID>>&seatID=<<"x:y">>&buyersName=<<name>>&email=<<email>>
//saves a ticket instance to the DB
//returns {<<ID>>}
app.get('/storeTicket', (req, res) => {

    const movieID = req.query.movieID;
    const showingID = req.query.showingID;
    const seatID = req.query.seatID;
    const buyersName = req.query.buyersName;
    const email = req.query.email;
    const price = req.query.ticketCost;

    ticketHandler.addTicket(movieID, showingID, seatID, buyersName, email, price, function (id) {
        if (id != -1) {
            const response = new ResponseClass();
            // send email with ticket
            const Emailer = new EmailerImport(
                process.env.EMAILER_HOST,
                process.env.EMAILER_PORT,
                process.env.EMAILER_USER,
                process.env.EMAILER_PASS
            );
            Emailer.sendEmailWithTicket({
                email: email,
                ticket: new ticketClass(movieID, showingID, seatID, buyersName, email, price)
            }, function (success, err) {
                if (success) {
                    // build response
                    response.setStatus(200);
                    response.setSuccessfulOperation(true);
                    response.setMessage("Ticket stored and sent to user.");
                    response.setData(id);

                    res.json(response.dictionary);
                }
                else {
                    // build response - email send failure
                    console.log(err);
                    response.setStatus(500);
                    response.setSuccessfulOperation(false);
                    response.setMessage("Ticket stored but failed to email to user.");
                    response.setData(id);

                    res.json(response.dictionary);
                }
            });
        }
        else {
            const response = new ResponseClass();
            response.setStatus(400);
            response.setSuccessfulOperation(false);
            response.setMessage("Ticket failed to store.");
            response.setData(-1);

            res.json(response.dictionary);
        }
    });

});

// on .../getTicketByID?ticketID=<<ID>>
// also returns a base64 string which can be decoded to a QR Code
app.get('/getTicketByID', (req, res) => {

    const ticketID = req.query.ticketID;

    ticketHandler.getTicketByID(ticketID, function (ticket) {

        if (ticket == false) {
            const response = new ResponseClass();
            response.setStatus(500);
            response.setSuccessfulOperation(false);
            response.setMessage("Failed to fetch ticket from DB");
            response.setData({});

            res.json(response.dictionary);
        }

        // generate qrcode base64 string
        var qrcode = null;
        QRCode.toDataURL(ticket.getStringRepresentation(), (err, url) => {

            if (err) { // error on QR Code generation
                const response = new ResponseClass();
                response.setStatus(500);
                response.setSuccessfulOperation(false);
                response.setMessage("Failed to generate QR Code");
                response.setData({});

                res.json(response.dictionary);
            }

            // base64 string, needs to be decoded on client to display image
            qrcode = url.substring(22); // skip base64 encoding header = 21 chars, on 22 the base64 string starts
            const data = {
                ticket: ticket,
                qrcode: qrcode
            }
            const response = new ResponseClass();
            response.setStatus(200);
            response.setSuccessfulOperation(true);
            response.setMessage("Returning ticket object");
            response.setData(data);

            res.json(response.dictionary);
        });
    });

});

//on .../addShowing?movieID=<<ID>>&screenNumber=<<screenNumber>>&startTime=<<startTime>
//adds a showing with the values passed to the showing table
//returns {'ShowingID': <<showingID>>}
app.get('/addShowing', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;

    var thisShowing = new showingClass(parseInt(qData.screenNumber), parseInt(qData.startTime));
    var response;

    thisShowing.setMovieID(qData.movieID);

    showingHandler.addShowing(thisShowing.getScreenNumber(), thisShowing.getStartTime(), thisShowing.getSeatMap(), thisShowing.getMovieID(), function (data) {
        thisShowing.setShowingID(data);
        movieHandler.updateShowings(qData.movieID, thisShowing.getShowingID(), function (data) {
            if (data == true) {
                response = new ResponseClass(200, true, 4, "Added showing", thisShowing.getShowingID());
                res.json(response.dictionary);
            }
        });
        if (thisShowing.getShowingID() == -1) {
            response = new ResponseClass(400, false, 4, "Failed to add showing", null);
            res.json(response.dictionary);
        }
    });
});

//on .../getShowingByID?showingID=<<ID>>
//gets a movie from the database with the ID passed
//returns {'ID': <<ID>>, 'ScreeNumber': <<screenNumber>>, 'StartTime': <<startTime>>, 'SeatMap': [[...], [...], ...], 'movieID': <<ID>>}
app.get('/getShowingByID', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;
    var response, thisShowing = -1;

    showingHandler.getShowingByID(qData.showingID, function (data) {
        thisShowing = data;
        response = new ResponseClass(200, true, 5, "Found showing", thisShowing);
        res.json(response.dictionary);
    }, function (e) {
        console.error(e);
        response = new ResponseClass(400, false, 5, "Failed to find showing", thisShowing);
        res.json(response.dictionary);
    });
});

//on .../getShowingsByDate?date=<<startTime>>
//gets all the showings that start in a 24 from the startTime
//returns {<<ID1>>: <<showing1>>, <<ID2>: <<showing2>>, ...}
app.get('/getShowingsByDate', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;
    var endTime = parseInt(qData.date) + 86400;
    var response, showings;

    showingHandler.getShowingsByTime(qData.date, endTime, function (data) {
        showings = data;
        response = new ResponseClass(200, true, 6, "Found showings", showings);
        res.json(response.dictionary);
    }, function (e) {
        console.error(e);
        response = new ResponseClass(400, false, 6, "Failed to find showings", thisShowing);
        res.json(response.dictionary);
    });
});

//on .../calcTicketCount?startDate=<<date>>&endDate=<<date>>
//gets the number of tickets sold in a given time period
//return {<<movie1>>:<<count>>, <<movie2>>:<<count>>, ...}
app.get('/calcTicketCount', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;
    var startDate = parseInt(qData.startDate);
    var endDate = parseInt(qData.endDate);
    var response;

    managementHandler.calcTicketCount(startDate, endDate, function (data) {
        counts = data;
        response = new ResponseClass(200, true, 7, "Calculated counts", counts);
        res.json(response.dictionary);
    }, function (e) {
        console.error(e);
        response = new ResponseClass(400, false, 7, "Failed to Calulate counts", counts);
        res.json(response.dictionary);
    });
});

//on .../calcTicketIncome?startDate=<<date>>&endDate=<<date>>
//gets the total value of tickets sold in a given time period
//return {<<movie1>>:<<count>>, <<movie2>>:<<count>>, ...}
app.get('/calcTicketIncome', (req, res) => {
    var q = url.parse(req.url, true);
    var qData = q.query;
    var startDate = parseInt(qData.startDate);
    var endDate = parseInt(qData.endDate);
    var response;
    var movieIncomes = {};
    var weeklyIncomes = {};
    var overall;
    var resList;

    managementHandler.calcTicketIncome(startDate, endDate, function (err, m, w, o) {
        if (err != "none") {
            response = new ResponseClass(500, false, 9, err, {});
            res.json(response.dictionary);
        }
        else {
            movieIncomes = m;
            weeklyIncomes = w;
            overall = o;
            resList = [m, w, o];
            response = new ResponseClass(200, true, 9, "Calculated income", resList);
            res.json(response.dictionary);
        }
    });
});

// Saves a users (UID) payment details in the DB
// USES POST AND SENDS A FORM DATA
app.post('/savePaymentData', (req, res) => {
    const data = req.body.params;
    const userID = data.userID;
    const cardNo = data.cardNo;
    const CVC = data.CVC;
    const expiryDate = data.expiryDate;
    let cardData = {
        cardNo: cardNo,
        CVC: CVC,
        expiryDate: expiryDate
    };

    if (paymentHandler.saveCard(userID, cardNo, CVC, expiryDate)) {
        let response = new ResponseClass(200, true, 11, "Saved card data", cardData);
        res.json(response.dictionary);
    }
    else {
        let response = new ResponseClass(400, false, 11, "Failed to save card data", cardData);
        res.json(response.dictionary);
    }



});

// Gets the card data associated with a userID
app.post('/getPaymentData', (req, res) => {

    const userID = req.body.params.userID;
    paymentHandler.returnCardInfo(userID, function (card) {
        const response = new ResponseClass();
        response.setStatus(200);
        response.setSuccessfulOperation(true);
        response.setMessage("Returning card info");
        response.setData(card);

        res.json(response.dictionary);
    });

});

 // Listen
 app.listen(port, () => console.log(`Server listening on ${port} ...`));
