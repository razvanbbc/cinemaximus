/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink, Link
  } from "react-router-dom";

import './screeningbutton.css';
import {getHourOfDay} from '../../../../Utils/Utils';
import {getMinuteOfHour} from '../../../../Utils/Utils';

class ScreeningButtonComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    // formats timestamp into 24 hour time e.g. 16:30
    

    render() {

        function formatTime(timestamp) { 
            var hours = getHourOfDay(timestamp);
            var minutes = getMinuteOfHour(timestamp);
    
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
    
            return (hours + ":" + minutes);
        }

        var ts = formatTime(this.props.timestamp);

        return (
            <Link className="movieLink" to={{
            pathname: "/movie",
            state: {selectedShowingID: this.props.id, selectedShowingTime: ts},
            }} onClick={() => {
                    this.props.clickHandler();
            }}>
                <div className="screeningButtonWrapper">
                    {ts}
                </div>
            </Link>
        );
    }

}

export default ScreeningButtonComponent;
