/*
*    This class will handle movies interactions with the database
*/

require('firebase');
const movieClass = require("../../../Classes/movie");

class MovieHandler {
	constructor(fbReference) {
		this.fbReference = fbReference;
		this.db = this.fbReference.database();
	}

	//attempts to adds a movie with the values passed to the database, generates a unique key for the movie instance
	//returns the ID of the movie in the database
	//if a movie with the same name already exists in the DB returns the ID of that movie
	async addMovie(movieName, showingIDs = [], MovieAPIID, callback) {
		var thisMovie = new movieClass(movieName, showingIDs, MovieAPIID);
		var movieID = "-1";
        var ref = this.db.ref("Movies");

		//Uniquely generated key to use as movieID
		movieID = ref.child("Movies").push().key;
		if (typeof movieID == "string" && movieID != "-1") {
			thisMovie.setMovieID(movieID);
			ref = this.db.ref("Movies/" + movieID);
			ref.set({
				'movieName': thisMovie.getMovieName(),
				'showingIDs': thisMovie.getShowingIDs(),
				'movieAPIID': thisMovie.getMovieAPIID()
            });
			callback(movieID);
		} else {
			console.log("failed to generate movieID in addMovie(movieName, showingIDs = [], MovieAPIID, callback)");
			callback(false);
        }
	}

	//takes a movieID
	//returns the matching movie instance in the database
	getMovieByID(movieID, callback) {
		var ref;
		var data;
        var thisMovie = "-1";
        if (typeof movieID == "string") { ref = this.db.ref("Movies/" + movieID) }
		else {
			console.log("movieID was not a string in getMovieByID(movieID, callback)")
			callback(false);
        }

        ref.on('value', (snap) => {
            try {
                data = snap.val();
                thisMovie = new movieClass(data.movieName, data.showingIDs, data.movieAPIID);
                thisMovie.setMovieID(snap.key);
                ref.off('value', undefined, this);
                callback(thisMovie);
            } catch {
                console.log("failed to get movie in getMovieByID(movieID, callback) ID: " + movieID);
                ref.off('value', undefined, this);
                callback(false);
            }
        });

    }

    //takes a movieName
    //retruns the matching movie instance in the database
    getMovieByName(movieName, callback) {
        var ref = this.db.ref("Movies");
        var data;
        var thisMovie = -1;

        ref.orderByChild("movieName").equalTo(movieName).on('child_added', function (snap) {
            try {
                data = snap.val();
                thisMovie = new movieClass(data.movieName, data.showingIDs, data.movieAPIID);
                thisMovie.setMovieID(snap.key);
                ref.off('child_added', undefined, this);
                callback(thisMovie);
            } catch {
                console.log("failed to get movie in getMovieByName(movieName, callback)");
                ref.off('child_added', undefined, this);
                callback(false);
            }
        });
	}

	//returns all movies in the database
    getAllMovies(callback) {
        var ref = this.db.ref("Movies");
        var movies = -1;

        ref.get().then(function (snap) {
            try {
                movies = snap.val();
                callback(movies);
            } catch {
                console.log("failed to get movies in getAllMovies(callback)")
                callback(false);
            }
        });

	}

	//takes a showing and updates a movies showing array with it
	updateShowings(movieID, newShowingID, callback) {
		var ref;
		var data, baseShowings = [], updateShowings = [];

		if (typeof movieID == "string") { ref = this.db.ref("Movies/" + movieID + '/showingIDs') }
		else {
			console.log("movieID was not a string in updateShowings(movieID, newShowingID, callback)")
			callback(false);
        }

        ref.get().then(function (snap) {
            try {
                data = snap.val();
                if (data != -1) {
                    baseShowings = data;
                    if (Array.isArray(baseShowings)) {
                        updateShowings = baseShowings;
                        updateShowings.push(newShowingID);
                    }
                    else {
                        updateShowings.push(baseShowings);
                        updateShowings.push(newShowingID);
                    }
                    ref.set(updateShowings);
                    callback(true);
                }
                else { callback(false) }
            } catch {
                console.log("failed to update movie showings in DB in updateShowings(movieID, newShowingID, callback)");
                callback(false);
            }
        });
    }
}

module.exports = MovieHandler;
