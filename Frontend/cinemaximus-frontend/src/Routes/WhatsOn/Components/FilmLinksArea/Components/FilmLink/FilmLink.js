/**
 * Imports -- modules, packages, assets
 */
import React from "react";
import {
    Link,
  } from "react-router-dom";

import './filmlink.css';

class FilmLinkComponent extends React.Component {

    constructor(props) {
        super(props);

        this.setStorage = this.setStorage.bind(this);
    }

    /** 
     * Saves the current chosen movie data to local storage 
     * so the detailed movie page can be refreshed
     */
    setStorage(params) {
        let savedState =  JSON.stringify(params);
        localStorage.setItem("currentMovieState", savedState);
    }

    render() {
        return (
            /** Pass data from specific movie query here */
            <Link className="movieLink" to={{
                pathname: "/movie",
                state: {},
            }} onClick={
                () => {
                    this.setStorage(this.props.details)}
                }
            >
            <div className="filmLinkWrapper">
                <div className="filmPosterWrapper">
                    <img src={this.props.details["poster"]} alt={this.props.details["movieName"] + " Poster"}/>
                </div>
                <div className="filmNameWrapper">
                    {this.props.details["movieName"]}
                </div>
            </div>
            </Link>
        );
    }
}

export default FilmLinkComponent;