/*
 * abstract for the user class implementations in backend and frontend
*/

/*
 * {
 *      int: userID         (primary key)
 *      string: name        (user's name)
 *      string: email       (user's email)
 *      int: phoneNo        (user's phone number)
 *      *int[]: ticketIDs   (ticket's the user has brought)
 * }
*/

class userAbstract
{
    constructor(userID, name, email, phoneNo)
    {
        if (this.constructor == userAbstract)
        {
            throw new Error("Cannot instanciate abstract class");
        }

        this.userID = userID;
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo
        this.tickets = [];
    }

    /** Getters */
    get dictionary() // returns the format above
    { 
        return {
            "userID": this.userID,
            "name": this.name,
            "email": this.email,
            "phoneNo": this.phoneNo,
            "tickets": this.tickets
        };
    }
}

module.exports = userAbstract;