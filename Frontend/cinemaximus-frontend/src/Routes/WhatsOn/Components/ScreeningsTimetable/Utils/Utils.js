// for functions that may need lots of reuse

const months = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
];

// takes date object, returns corresponding day as string
export function getDayOfWeek(date) {
  return days[date.getDay()];
}
// takes date object, returns corresponding month as string
export function getMonthOfYear(date) {
  return months[date.getMonth()];
}

// takes unix timestamp (seconds), returns hour value
export function getHourOfDay(timestamp) {
  var date = new Date(timestamp * 1000);
  var hours = date.getHours();
  return hours;
}

// takes unix timestamp (seconds), returns minute value
export function getMinuteOfHour(timestamp) {
  var date = new Date(timestamp * 1000);
  var minutes = date.getMinutes();
  return minutes;
}