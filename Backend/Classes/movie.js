/*
 * concrete implementation of the movie class for backend
*/

/*
 * {
 *      string: movieID            (primary key)
 *      string: movieName       (name of the movie)
 *      *string[]: showingIDs      (list of showings)
 *      int: movieAPIID      (primary key of movie in IMDb)
 * }
*/

const Abstract = require("../../Class Abstracts/movieAbstract");

class Movie extends Abstract {
    constructor(movieName, showingIDs = [], movieAPIID = -1) {
        if (typeof movieName == "string") { super(0, movieName) }
        else { super(0, "Unknown") }

        if (typeof showingIDs == "string") {
            this.showingIDs = [];
            this.showingIDs.push(showingIDs);
        }
        else if ( Array.isArray(showingIDs) == true) { this.showingIDs = showingIDs }
        else { this.showingIDs = [] }

        if (Number.isInteger(movieAPIID) == true) { this.movieAPIID = movieAPIID }
        else { this.movieAPIID = -1 }
    }

    setMovieID(movieID) {
        if (typeof movieID == "string") { this.movieID = movieID }
        else { console.log("movieID must be a string in setMovieID(movieID)") }
    }

    setShowingIDs(showingIDs) {
        this.showingIDs = [];
        if (typeof showingIDs == "string") { this.showingIDs.push(showingIDs) }
        else if (Array.isArray(showingIDs) == true) {
            for (var i = 0; i < showingIDs.length; i++) {
                if (typeof showingIDs[i] == "string") { this.showingIDs.push(showingIDs[i]) }
                else { console.log("Element " + showingIDs[i] + " in showingIDs is not a string") }
            }
        }
        else { console.log("showingIDs must be a string or array in setShowingIDs(showingIDs)") }
    }

    setMovieAPIID(movieAPIID) {
        if (Number.isInteger(movieAPIID) == true) { this.setMovieAPIID = movieAPIID }
        else { console.log("movieAPIID must be a integer in setMovieAPIID(movieAPIID)") }
    }

    addShowing(showingIDs) {
        if (typeof showingIDs == "string") { this.showingIDs.push(showingIDs[i]) }
        else if (Array.isArray(showingIDs) == true) {
            for (var i = 0; i < showingIDs.length; i++) {
                if (typeof showingIDs[i] == "string") { this.showingIDs.push(showingIDs[i]) }
                else { console.log("Element " + showingIDs[i] + " in showingIDs is not a string") }
            }
        }
        else { console.log("showingIDs must be a string or array in addShowing(thisShowing)") } 
    }

    getMovieID() { return this.movieID }

    getMovieName() { return this.movieName }

    getShowingIDs() { return this.showingIDs }

    getMovieAPIID() { return this.movieAPIID }
} 

module.exports = Movie;