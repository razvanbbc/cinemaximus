/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink,
  } from "react-router-dom";

import "./cardpaymentform.css";
import UserProfile from "../../../UserArea/UserProfile";
import Client from "../../../../HTTP/Client/Client";

class CardPaymentFormComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cardNumber: "",
            expiry: "",
            cvc: "",
            saved_details: {},
        };

        this.takeInput = this.takeInput.bind(this);
        this.update = this.update.bind(this);
        this.useSavedDetails = this.useSavedDetails.bind(this);
    }

    componentDidMount() {
        const userProfile = UserProfile.getData();
        if (userProfile === undefined || userProfile.id == "") { // user not logged in
            this.setState({saved_details: null});
            return;
        } else {

            new Promise((resolve, reject) => {
                const client = new Client("http://localhost:8080");
                client.requestDataFromEndpoint("/getPaymentData", "post", {userID: userProfile.id}, function(response) {
                    resolve(response.data);
                });
            }).then((data) => {
                this.setState({
                    saved_details: 
                    {
                    cardNumber: data.cardNumber,
                    expiry: data.expiryDate,
                    cvc: data.CVC,
                    }
                });
                return;
            });
        }
    }

    useSavedDetails() {
        var cardDetails = this.state.saved_details;
        console.log(cardDetails);
        var _this = this;
        if (cardDetails) {
            this.setState({
                cardNumber: cardDetails.cardNumber,
                expiry: cardDetails.expiry,
                cvc: cardDetails.cvc,
            }, () => {
                var cardNumberInput = document.getElementById("card-number-input");
                var expiryInput = document.getElementById("card-expiry-input");
                var cvcInput = document.getElementById("card-sc-input");
                _this.update("cardNumber", {key: "", keyCode: -1, target: cardNumberInput});
                _this.update("expiry", {key: "", keyCode: -1, target: expiryInput});
                _this.update("cvc", {key: "", keyCode: -1, target: cvcInput});
            });
        }
    }

    update(field, event) {
        switch(field) {
            case "cardNumber":
                var newCardNumber = this.state.cardNumber;
                if (event.key == "Backspace") {
                    if (newCardNumber.length > 0) {
                        newCardNumber = newCardNumber.substring(0, newCardNumber.length - 1);
                    }
                } else if (event.keyCode >= 48 && event.keyCode <= 57) {
                    if (newCardNumber.length < 16) {
                        newCardNumber = newCardNumber + event.key;
                    }
                }
                event.target.value = newCardNumber.replace(/(.{4})/g, '$1 ').trim();
                this.props.updateState(newCardNumber, this.state.expiry, this.state.cvc);
                this.setState({cardNumber: newCardNumber});
                break;
            case "expiry":
                var newExpiry = this.state.expiry;
                if (event.key == "Backspace") {
                    if (this.state.expiry.length > 0) {
                        newExpiry = newExpiry.substring(0, newExpiry.length - 1);
                    }
                } else if (event.keyCode >= 48 && event.keyCode <= 57) {
                    if (newExpiry.length < 4) {
                        newExpiry = newExpiry + event.key;
                    }
                }
                event.target.value = newExpiry.replace(/(.{2}?)/g, '$1/').substring(0, 5);
                this.props.updateState(this.state.cardNumber, newExpiry, this.state.cvc);
                this.setState({expiry: newExpiry});
                break;
            case "cvc":
                var newCVC = this.state.cvc;
                if (event.key == "Backspace") {
                    if (newCVC.length > 0) {
                        newCVC = newCVC.substring(0, newCVC.length - 1);
                    }
                } else if (event.keyCode >= 48 && event.keyCode <= 57) {
                    if (newCVC.length < 3) {
                        newCVC = newCVC + event.key;
                    }
                }
                event.target.value = newCVC;
                this.props.updateState(this.state.cardNumber, this.state.expiry, newCVC);
                this.setState({cvc: newCVC});
                break;
            default:
                break;
        }
    }

    takeInput(field, event) {
        event.preventDefault();
        this.update(field, event);
    }

    render() {
        return (
            <div className="card-payment-form-wrapper">
                <form className="card-payment-form">
                    <div className="card-payment-form-line">
                        <div className="card-payment-input-wrapper">
                            CARD NUMBER
                            <br/>
                            <input onKeyDown={(e) => this.takeInput("cardNumber", e)} id="card-number-input" className="card-payment-input" type="text" maxLength="16" placeholder="Valid Card Number" autoComplete="off"/>
                        </div>
                    </div>
                    <div className="card-payment-form-line">
                        <div className="card-payment-input-wrapper">
                            EXPIRY DATE
                            <br/>
                            <input onKeyDown={(e) => this.takeInput("expiry", e)} id="card-expiry-input" className="card-payment-input" type="text" maxLength="4" placeholder="MM/YY" autoComplete="off"/>
                        </div>
                        <div className="card-payment-input-wrapper">
                            CV CODE
                            <br/>
                            <input onKeyDown={(e) => this.takeInput("cvc", e)} id="card-sc-input" className="card-payment-input" type="text" maxLength="3" placeholder="CVC" autoComplete="off"/>
                        </div>
                    </div>
                </form>
                <button className="use-saved-details-button" onClick={this.useSavedDetails}>Use Saved Card Details</button>
            </div>
        );
    }

}

export default CardPaymentFormComponent;
