/**
 * Class which represents the structure of the responses sent by the API
 */

 /**
  * {   
  *     Status : Integer (common status codes)
  *     Successful : Boolean
  *     Operation : Integer (1 - n), to be decided
  *     Message : String
  *     Data : { Any } (?)
  * }
  */

 class RestResponse {

    constructor(statusCode, successfulOperation, operation, message, data) {
        this.statusCode = statusCode;
        this.successfulOperation = successfulOperation;
        this.operation = operation;
        this.message = message;
        this.data = data; // will have to be properly built right before sending the response to the client
    }

    /** Getters */
    get dictionary() { // returns the format above
        return {
            "statusCode": this.statusCode,
            "successfulOperation": this.successfulOperation,
            "operation": this.operation,
            "message": this.message,
            "data": this.data
        };
    }

    /** Setters */
    setStatus(statusCode) {
        this.statusCode = statusCode;
    }
    setSuccessfulOperation(successfulOperation) {
        this.successfulOperation = successfulOperation;
    }
    setOperations(operation) {
        this.operation = operation;
    }
    setMessage(message) {
        this.message = message;
    }
    setData(data) {
        this.data = data;
    }

 }

 export default RestResponse;