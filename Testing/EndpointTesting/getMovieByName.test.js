/********************* Requirements **********************/
const fetch = require("node-fetch");

//Tests the getMovieTMDB endpoint
it("Getting a Movie by Name", async () => {
    var endpointAddress = "http://localhost:8080/getMovieByName?movieName=Soul"

    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data.thisMovie.movieName).toEqual('Soul');
});