/*
*    This class will handle payments interactions with the database
*/

require('firebase');
const paymentClass = require("../../../Classes/payment");

class PaymentHandler {
    constructor(fbReference) {
        this.fbReference = fbReference;
        this.db = this.fbReference.database();
    }

    //adds a payment with the values passed to the database, generates a unique key for the movie instance
    //returns the ID of the payment in the database
    addPayment(cardName, cardNo, securityDigits, callback) {
        var thisPayment = new paymentClass(cardName, cardNo, securityDigits);
        var paymentID = "-1";
        var ref = this.db.ref("Payments");

        //Uniquely generated key to use as paymentID
        paymentID = ref.child("Payment").push().key;
        if (typeof paymentID == "string" && paymentID != "-1") {
            thisPayment.setPaymentID(paymentID);
            ref = this.db.ref("Payments/" + paymentID);
            ref.set({
                'cardName': thisPayment.getCardName(),
                'cardNo': thisPayment.getCardNo(),
                'securityDigit': thisPayment.getSecurityDigits()
            });
            callback(paymentID);
        } else {
            console.log("failed to generate paymentID in addPayment(cardName, cardNo, securityDigits, callback)");
            callback(false);
        }
    }

    //takes a payment ID
    //returns the matching payment instance in the database
    getPaymentByID(paymentID, callback) {
        var ref;
        var data;
        var thisPayment = -1;

        if (typeof paymentID == "string") { ref = this.db.ref("Payments/" + paymentID) }
        else {
            console.log("paymentID was not a string in getPaymentByID(paymentID, callback)");
            callback(false);
        }

        ref.on('value', (snap) => {
            try {
                data = snap.val();
                thisPayment = new paymentClass(data.cardName, data.cardNo, data.securityDigit);
                thisPayment.setPaymentID(snap.key);
                ref.off('value', undefined, this);
                callback(thisPayment);
            } catch {
                console.log("failed to get payment in getPaymentByID(paymentID, callback)");
                ref.off('value', undefined, this)
                callback(false);
            }
        });
    }

	// Function to check whether the entered card number could be a valid card
	// Not connected to banks so can't check for sure, but checks if the format is correct
	// Returns true/false
	checkCard(cardNo, securityDigits, expiryDate) {
    	// Integer comparison to check if month and year are valid and later than June 2021
		let dateArray = expiryDate.split("/");
		if (parseInt(dateArray[0]) > 12 || parseInt(dateArray[0]) < 1) {
			return false;
		}
		// Debit/Credit cards typically expire after 2-3 years, hence the max limit
		if (parseInt(dateArray[1]) < 2021 || parseInt(dateArray[1]) > 2024) {
			return false;
		}
		if (securityDigits < 100 || securityDigits > 9999) {
			return false;
		}
		if (cardNo[0] < 1000 || cardNo[0] > 9999) {
			return false;
		}
		for (let i = 1; i < 4; i++) {
			if (cardNo[i].toString().length != 4) {
				return false;
			}
		}
		// If none of the checks are true (we haven't returned false) we return true.
		return true;
	}

	// Saves card corresponding to a user id to the Database
    saveCard(userID, cardNo, securityDigits, expiryDate) {
        const ref = this.db.ref('Users');

        let cardData = {
            cardNumber: cardNo,
            CVC: securityDigits,
            expiryDate: expiryDate
        };
        try {
            ref.child(userID).set(cardData);
            return true;
        }
        catch {
            return false;
        }
    }

    // Returns the card info associated with a userID
    returnCardInfo(userID, callback) {

        let data;
        let thisCard;

        const ref = this.db.ref('Users/' + userID);

        ref.on('value', (snap) => {
            try {
                data = snap.val();
                thisCard = {
                    cardNumber: data.cardNumber,
                    CVC: data.CVC,
                    expiryDate: data.expiryDate
                };
                ref.off('value', undefined, this);
                callback(thisCard);
            } catch {
                console.log("failed to get card in returnCardInfo(userID, callback)");
                ref.off('value', undefined, this);
                callback(false);
            }
        });


    }

}

module.exports = PaymentHandler;
