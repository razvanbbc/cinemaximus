/**
 * Imports -- modules, packages, assets
 */
import React from "react";
import {
    Link,
  } from "react-router-dom";

import './FilmLinksArea.css';
import FilmLinkComponent from "./Components/FilmLink/FilmLink";
import CycleButtonComponent from "./Components/CycleButton/CycleButton";
import Client from "../../../../HTTP/Client/Client";

class FilmLinksAreaComponent extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {
            index: 0,
            links: [],
        };

        this.changeIndex = this.changeIndex.bind(this);
        this.decrementIndex = this.decrementIndex.bind(this);
        this.incrementIndex = this.incrementIndex.bind(this);
    }

    componentDidMount() {
        this.getLinks = this.getLinks.bind(this);
        this.setLinks = this.setLinks.bind(this);
        this.addFilmstoLocalStorage = this.addFilmstoLocalStorage.bind(this);
        this.getLinks();
    }

    componentDidUpdate() {
        if (this.state.links.length == 0) {
            this.getLinks();
        }
    }

    addFilmstoLocalStorage(details) {
        let savedState =  JSON.stringify(details);
        localStorage.setItem("allMovies", savedState);
    }

    setLinks(details) {
        localStorage.removeItem("allMovies");
        var links = [];
        for (let i = 0; i < details.length; i++) {
            if (details[i]["poster"] != "TIMEDOUT" && details[i]["poster"] != "FAILED") { // we found the poster!
                // might want to show a default image rather than excluding the filmlink altogether if poster isnt found
                links.push(<FilmLinkComponent details={details[i]} />);
            }
        }
        this.setState({
            links: links,
        });
        this.addFilmstoLocalStorage(details);
        //this.state.links = links;
    }

    // generates list of FilmLinkComponents from list of films (need to figure out posters)
    getLinks() {

        localStorage.removeItem("allMovies");
        var _this = this; // strong reference to this
        const client = new Client("http://localhost:8080");

        const data = {};

        var details = []; // array to store raw return data from getAllMovies
        var ids = [];
        var names = []; // array to store name of movie
        var posters = []; // array to store paths to posters

        var promises;

        var p = new Promise((resolve, reject) => {
            client.requestDataFromEndpoint("/getAllMovies", "get", data, function(response) {
                for (var key in response.data) {
                    if (response.data[key].movieAPIID == -1) {
                        continue; // skip this, as its not a real movie
                    }
                    var deets = response.data[key];
                    deets["id"] = key;
                    details.push(deets);
                }
                resolve(1);
            });
        });


        p.then(() => { // once we have got all the movies
            var q = [];
            for (var movieDetail in details) {
                const data2 = {
                    filmAPIID:details[movieDetail].movieAPIID.toString(),
                };
                q.push(new Promise((resolve, reject) => {
                    client.requestDataFromEndpoint("/getMovieTMDB", "get", data2, function(tmdbResponse) {
                        try {
                            resolve([tmdbResponse.data.PosterPath, tmdbResponse.data.ReleaseDate, tmdbResponse.data.Genres]);
                        } catch (err) {
                            resolve("FAILED");
                        }
                    }, setTimeout(function() {
                        resolve("TIMEDOUT")
                    }, 5000));
                }));
            }

            Promise.all(q).then(data => { // once we have had response from all getMovieTMDBs
                for (var i = 0; i < data.length; i++) {
                    details[i]["poster"] = data[i][0];
                    details[i]["releaseDate"] = data[i][1];
                    details[i]["genres"] = data[i][2];
                }
                this.setLinks(details);
            });
        });
    }

    // scrolls films left or right, with wrap-around
    changeIndex(diff) {
        var newIndex = (this.state.index + diff) % this.state.links.length;
        if (newIndex < 0) {
            newIndex += this.state.links.length;
        }
        this.setState({
            index: newIndex,
        });
    }

    decrementIndex() {
        this.changeIndex(-1);
    }

    incrementIndex() {
        this.changeIndex(1);
    }

    render() {

        var content;

        if (this.state.links.length == 0) { // an alternative to the 'timeout' method
            content = "Loading";
        } else {
            content = <>
            {this.state.links[this.state.index]}
            {this.state.links[(this.state.index + 1) % this.state.links.length]}
            {this.state.links[(this.state.index + 2) % this.state.links.length]}
            </>;
        }

        return (
            <div className="filmLinksWrapper">
                <CycleButtonComponent symbol="<" handleClick={this.decrementIndex}/>
                {content}
                <CycleButtonComponent symbol=">" handleClick={this.incrementIndex}/>
            </div>
        );
    }
}

export default FilmLinksAreaComponent;