
var UserProfile = (function() {

    var userData = {
        id: localStorage.user_info == undefined ? "" : JSON.parse(localStorage.user_info).id,
        name: localStorage.user_info == undefined ? "" : JSON.parse(localStorage.user_info).name,
    };

    const requiredData = ["id", "name"];

    // success = 1, failure = -1 
    var setData = function(data) {
        for (var key of requiredData) {
            if (!(key in data)) {
                console.error("Missing data in UserProfile setData parameter (" + key +")")
                return -1;
            }
        }
        for (var key of requiredData) {
            userData[key] = data[key];
            localStorage.setItem("user_info", JSON.stringify(data));
        }
        return 1;
    }

    var getData = function() {
        return userData;
    }
  
    return {
      getData: getData,
      setData: setData,
    }
  
  })();
  
  export default UserProfile;