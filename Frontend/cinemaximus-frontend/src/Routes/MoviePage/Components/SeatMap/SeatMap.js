/**
 * Imports -- modules, packages, assets
 */
import React from "react";

import SeatButtonComponent from './Components/SeatButton/SeatButton';
import './seatmap.css';

class SeatMapComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            buttons: [], // list of all buttons (array of seats)
            selected: [], // list of selected seats
            selecting: 0, // are we selecting or deselecting?
        };
        this.setSelecting = this.setSelecting.bind(this);
        this.getSelecting = this.getSelecting.bind(this);
        this.clearSelecting = this.clearSelecting.bind(this);
        this.toggleSeat = this.toggleSeat.bind(this);
        this.makeSeatButtons(props.seats);
    }

    setSelecting(value) {
        this.setState({selecting: value});
    }
    clearSelecting(event) {
        if (event.target.className == "seat-map-wrapper" || event.type == "mouseup") {
            this.setSelecting(0);
        }
    }

    toggleSeat(row, col) {
        var seats = [...this.state.selected];
        var index = -1;
        for (var i = 0; i < seats.length; i++) {
            var seat = seats[i];
            if (seat[0] == row && seat[1] == col) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            seats.push([row, col]);
        } else {
            seats.splice(index, 1);
        }
        this.setState({
            selected: seats,
        });
        this.props.selectionChange(seats);
    }

    getSelecting() {
        return this.state.selecting;
    }

    // takes 2d array of seats from props
    makeSeatButtons(seats) {
        for (var i = 0; i < seats.length; i++) {
            this.state.buttons.push([]);
            for (var j = 0; j < seats[0].length; j++) {
                this.state.buttons[i].push(
                <SeatButtonComponent key={i * seats.length + j} row={i} col={j} enabled={!seats[i][j]} vip={i == 4||i==5} getSelecting={this.getSelecting} setSelecting={this.setSelecting} toggle={this.toggleSeat}/>
                );
            }
        }
    }

    render() {

        var seatGrid = [];
        for (var i = 0; i < this.state.buttons.length; i++) {
            seatGrid.push(
                <div key={i} className="seat-map-row">
                    {this.state.buttons[i]}
                </div>
            );
        }

        var i;
        var rows = [];
        for (i = 0; i < 4; i++) {
            rows.push(seatGrid[i]);
        }
        var standardSection1 = <div className="standard-seat-rows">{rows}</div>;
        rows = [];
        for (i = 4; i < 6; i++) {
            rows.push(seatGrid[i]);
        }
        var vipSection = <div className="vip-section-wrapper">
            <div className="vip-seat-rows">
                {rows}
            </div>
            <div className="section-label">
                <div className="section-label-table">
                    <div className="section-label-table-cell">
                        <div>
                            VIP
                        </div>
                    </div>
                </div>
            </div>
        </div>;
        rows = [];
        for (i = 6; i < seatGrid.length; i++) {
            rows.push(seatGrid[i]);
        }
        var standardSection2 = <div className="standard-seat-rows">{rows}</div>;

        return (
            <div className="seat-map-wrapper" onMouseOut={this.clearSelecting} onMouseUp={this.clearSelecting}>
                {standardSection1}
                {vipSection}
                {standardSection2}
            </div>
        );
    }

}

export default SeatMapComponent;