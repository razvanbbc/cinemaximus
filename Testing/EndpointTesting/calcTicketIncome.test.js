/********************* Requirements **********************/
const fetch = require("node-fetch");

var movieID = null;
var showingID1 = null;
var showingID2 = null;

//Tests the getMovieTMDB endpoint
it("Getting Showing by Date", async () => {
    var endpointAddress = "http://localhost:8080/calcTicketIncome?startDate=1615127000&endDate=1616606000";
    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data.testManMovie1).toEqual(55);
    expect(json.data.testManMovie2).toEqual(30);
});