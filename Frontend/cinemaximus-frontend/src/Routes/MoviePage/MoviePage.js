/**
 * Imports -- modules, packages, assets
*/
import React, { useRef } from "react";
import Movie from "./assets/Classes/Movie";
import {
    Link,
} from "react-router-dom";
import SlideToggle from "react-slide-toggle";
import OperationMessage from "./Components/OperationMessage/OperationMessage";
import SeatMapComponent from './Components/SeatMap/SeatMap';
import ShowingButtonComponent from "./Components/ShowingButton/ShowingButton";
import CardPaymentForm from "./Components/CardPaymentForm/CardPaymentForm";

import "./assets/MoviePage.css";
import arrowIcon from "./assets/arrow.png";

import DayTabComponent from "../WhatsOn/Components/ScreeningsTimetable/Components/DayTab/DayTab";

/** HTTP Client Logic */
import Client from "../../HTTP/Client/Client";
import CardPaymentFormComponent from "./Components/CardPaymentForm/CardPaymentForm";

/** User Profile Data */
import UserProfile from "../UserArea/UserProfile";
 
/**
 * Extra-detailed view of a MoviePage initialised with params from clicked component 
*/
class MoviePage extends React.Component {

    constructor(props) { // movie object ?

        super(props);
        // save props to localstorage so refreshing works on moviepage
        let retrievedState = JSON.parse(localStorage.getItem("currentMovieState"));
        this.movie = new Movie(retrievedState["id"], retrievedState["movieName"], retrievedState["releaseDate"], retrievedState["poster"], retrievedState["showingIDs"], retrievedState["movieAPIID"], retrievedState["genres"]);
        // state vars, updated through input field text
        this.state = {
            name: "",
            email: "",
            ticketCost: 0.00.toFixed(2),
            successfulOp: undefined,
            showings: [],
            selectedShowingTime: "",
            selectedShowingID: "",
            selectedShowingSeatMap: undefined,
            showingDay: 0,
            adultTickets: 0,
            childTickets: 0,
            vipTickets: 0,
            cardNumber: "",
            expiry: "",
            cvc: "",
            bookingSuccessMessage: "",
            formErrorMessage: "",
            seatsErrorMessage: "",
            paymentErrorMessage: "",
            seatsSelected: [],
            toggleShowing: 0,
            toggleForm: 0,
            toggleSeats: 0,
            togglePayment: 0,
            booking: false,
        }

        this.adultCost = 9.99;
        this.childCost = 4.99;
        this.vipCost = 14.99;

        this.scrollRef = React.createRef();
        this.executeScroll = this.executeScroll.bind(this);

        // let functions access the class instance
        this.updateInput = this.updateInput.bind(this);
        this.pickShowing = this.pickShowing.bind(this);
        this.submitForm = this.submitForm.bind(this);
        this.bookingSummary = this.bookingSummary.bind(this);
        this.seatSelectionChange = this.seatSelectionChange.bind(this);
        this.getShowings = this.getShowings.bind(this);
        this.sortShowingsByDay = this.sortShowingsByDay.bind(this);
        this.setActiveTab = this.setActiveTab.bind(this);
        this.makeShowingButtons = this.makeShowingButtons.bind(this);
        this.goToPayment = this.goToPayment.bind(this);
        this.takePaymentInput = this.takePaymentInput.bind(this);
        this.attemptBooking = this.attemptBooking.bind(this);
        this.finishedBookingAttempt = this.finishedBookingAttempt.bind(this);
        this.displayGenres = this.displayGenres.bind(this);
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getShowings();

        if (this.props.location.state != undefined) {
            if (this.props.location.state["selectedShowingID"] != undefined) {
                this.pickShowing(this.props.location.state["selectedShowingID"], this.props.location.state["selectedShowingTime"]);
                this.executeScroll();
            }
        }
    }
    executeScroll = () => this.scrollRef.current.scrollIntoView();

    /**
     * Displays the genres attached to the movie object of this movie page
     */
    displayGenres() {
        var genres = "";
        for (var i = 0; i < this.movie.genres.length; i++) {
            genres += `${this.movie.genres[i]}, `
        }
        return genres;
    }

    getShowings() {
        const client = new Client("http://localhost:8080");

        var showings = [];

        var promises = [];

        var _this = this;
        for (var id of this.movie.showingIDs) {
            promises.push(new Promise((resolve, reject) => {
                client.requestDataFromEndpoint("/getShowingByID", "get", {showingID: id}, function (response) {
                    if (!response.data) {
                        resolve("FAILED");
                    }
                    showings.push(response.data);
                    resolve(response.data);
                });
            }));
        }
        Promise.all(promises).then(() => {
            _this.sortShowingsByDay(showings);
        });
    }

    // DATE OBJECTS ARE IN MILLISECONDS, OUR DATABASE IS IN SECONDS
    sortShowingsByDay(showings) {
        var secondsPerDay = 3600 * 24;
        var startOfToday = new Date();
        var now = Date.now();
        startOfToday.setHours(0, 0, 0, 0);

        var showingsByDay = [[], [], [], [], [], [], []];

        for (var showing of showings) {
            if (showing["startTime"] == undefined || showing["startTime"] == -1) {
                continue;
            }
            var startTime = new Date(showing["startTime"] * 1000);
            var diff = (startTime.getTime() - startOfToday.getTime()) / 1000;
            var days = Math.floor(diff / secondsPerDay);
            if (days < 0 || days > 6 || startTime < now) { // too far in future or in past
                continue;
            }
            showingsByDay[days].push(showing);
        }

        function compareShowings(a, b) {
            var ta = a["startTime"];
            var tb = b["startTime"];

            if (ta < tb) {
                return -1;
            } else return 1;
        }

        for (var i = 0; i < 7; i++) {
            if (showingsByDay[i] == undefined) {
                continue;
            }
            showingsByDay[i].sort(compareShowings);
        }

        this.setState({
            showings: showingsByDay,
        });
    }

    /**
     * Gets called when there are changes in an input field
     * @param {string} field = the input field identifier
     * @param {e} event
     */
    updateInput(field, event) {

        switch (field) {
            case "name":
                this.setState({name: event.target.value});
                break;
            case "email":
                this.setState({email: event.target.value});
                break;
            case "adultTickets":
                try {
                    var num = parseInt(event.target.value);
                    var newCost = parseInt(num) * this.adultCost + parseInt(this.state.childTickets) * this.childCost + parseInt(this.state.vipTickets) * this.vipCost;
                    this.setState({
                        adultTickets: num,
                        ticketCost: newCost.toFixed(2),
                    });
                } catch (err) {
                    event.target.value = "";
                    this.setState({adultTickets: event.target.value});
                }
                break;
            case "childTickets":
                try {
                    var num = parseInt(event.target.value);
                    var newCost = parseInt(this.state.adultTickets) * this.adultCost + parseInt(num) * this.childCost + parseInt(this.state.vipTickets) * this.vipCost;
                    this.setState({
                        childTickets: num,
                        ticketCost: newCost.toFixed(2),
                    });
                } catch (err) {
                    event.target.value = "";
                    this.setState({childTickets: event.target.value});
                }
                break;
            case "vipTickets":
                try {
                    var num = parseInt(event.target.value);
                    var newCost = parseInt(this.state.adultTickets) * this.adultCost + parseInt(this.state.childTickets) * this.childCost + parseInt(num) * this.vipCost;
                    this.setState({
                        vipTickets: num,
                        ticketCost: newCost.toFixed(2),
                    });
                } catch (err) {
                    event.target.value = "";
                    this.setState({vipTickets: event.target.value});
                }
                break;
            default:
                // NOTHING HERE
        }
    }
 
    /** 
     * Gets called when pressing the 'Book' button
     * Sends the current state of the inputs to the backend for storage and gets a TicketID in return
     */

    isValidForm() {
        if (!this.checkIfEmpty(this.state.name) || 
            !this.checkIfEmpty(this.state.email)) {
            this.setState({
                formErrorMessage: "Please fill in all fields!",
            });
            return false;
        }

        var emailRegex = new RegExp(/^[^@\s]+@[^@\s.]+.[^@\s]+$/);
        if (!emailRegex.test(this.state.email)) {
            this.setState({
                formErrorMessage: "Invalid email address!",
            });
            return false;
        }

        if (this.state.adultTickets < 0 || this.state.childTickets < 0 || this.state.vipTickets < 0) {
            this.setState({
                formErrorMessage: "Invalid ticket number!",
            });
            return false;
        }
        if (this.state.adultTickets == 0 && this.state.childTickets == 0 && this.state.vipTickets == 0) {
            this.setState({
                formErrorMessage: "No tickets requested!",
            });
            return false;
        }
        return true;
    }

    checkSeats() {

        var numChild = parseInt(this.state.childTickets);
        var numAdult = parseInt(this.state.adultTickets);
        var numVIP = parseInt(this.state.vipTickets);

        if (this.state.seatsSelected.length < numAdult + numChild + numVIP) {
            this.setState({seatsErrorMessage: "Too few seats selected!"});
            return false;
        }
        if (this.state.seatsSelected.length > numChild + numAdult + numVIP) {
            this.setState({seatsErrorMessage: "Too many seats selected!"});
            return false;
        }

        var vipCount = 0;
        var nonVIPCount = 0;

        for (var seat of this.state.seatsSelected) {
            if (seat[0] == 4 || seat[0] == 5) {
                vipCount++;
            } else {
                nonVIPCount++;
            }
        }

        if (vipCount < numVIP) {
            this.setState({seatsErrorMessage: "Too few VIP seats selected!"});
            return false;
        }
        if (vipCount > numVIP) {
            this.setState({seatsErrorMessage: "Too many VIP seats selected!"});
            return false;
        }
        if (nonVIPCount < numAdult + numChild) {
            this.setState({seatsErrorMessage: "Too few non-VIP seats selected!"});
            return false;
        }
        if (nonVIPCount > numAdult + numChild) {
            this.setState({seatsErrorMessage: "Too many non-VIP seats selected!"});
            return false;
        }

        this.setState({seatsErrorMessage: ""});
        return true;
    }

    /**
     * Useful for input sanitising
     */
    checkIfEmpty(data) {
        if (data === undefined) return false;
        else if (data == "") return false;
        return true;
    }


    pickShowing(showing, timeString) {
        var date = new Date();
        date.setDate(date.getDate() +  + this.state.showingDay);
        
        var day = (date.getDate());
        if (day < 10) {
            day = "0" + day;
        }
        var month = (date.getMonth() + 1);
        if (month < 10) {
            month = "0" + month;
        }
        var dateString = day + "/" + month;
        this.setState({
            toggleShowing: Date.now(),
            toggleForm: Date.now(),
            selectedShowingID: showing,
            selectedShowingTime: timeString + " " + dateString,
        });

        var _this = this;

        new Promise((resolve, reject) => {
            var client = new Client("http://localhost:8080");
            client.requestDataFromEndpoint("/getShowingByID", "get", {showingID: showing}, function(response) {
                resolve(response.data);
            });
        }).then((data) => {
            _this.setState({
                selectedShowingSeatMap: data["seatMap"],
            });
        });
    }

    bookingSummary() {
        var summary;
        var showingPicked = <></>;
        var name = <></>;
        var email = <></>;
        var numAdult = <></>;
        var numChild = <></>;
        var numVIP = <></>;
        var price = <></>;
        var seats = <></>;
        var seatsList = "";

        if (this.state.selectedShowingTime != "") {
            showingPicked = <><span>{"Showing: " + this.state.selectedShowingTime}</span><br/></>;
            price = <><span>{"Price: £" + this.state.ticketCost}</span><br/></>;
        }
        if (this.state.toggleSeats != 0) {
            name = <><span>{"Name: " + this.state.name}</span><br/></>;
            email = <><span>{"Email: " + this.state.email}</span><br/></>;
            if (this.state.adultTickets != 0) {
                numAdult = <><span>{"Adults: " + this.state.adultTickets}</span><br/></>;
            }
            if (this.state.childTickets != 0) {
                numChild = <><span>{"Children: " + this.state.childTickets}</span><br/></>;
            }
            if (this.state.vipTickets != 0) {
                numVIP = <><span>{"VIP: " + this.state.vipTickets}</span><br/></>;
            }
            if (this.state.seatsSelected.length == 0) {
                seats = <><span>{"Seats Selected: None"}</span></>;
            } else {
                for (var i = 0; i < this.state.seatsSelected.length; i++) {
                    var seat = this.state.seatsSelected[i];
                    seatsList += String.fromCharCode(65 + seat[0]) + seat[1];
                    if (i != this.state.seatsSelected.length - 1) {
                        seatsList += ", ";
                    }
                }
                seats = <><span>{"Seats Selected: " + seatsList}</span></>;
            }
        }
        summary = <div className="booking-summary-wrapper">
            {showingPicked}
            {name}
            {email}
            {numAdult}
            {numChild}
            {numVIP}
            {price}
            {seats}
        </div>;
        return summary;
    }

    goToPayment() {
        if (!this.checkSeats()) {
            return;
        }
        this.setState({
            toggleSeats: Date.now(),
            togglePayment: Date.now(),
        });
    }

    takePaymentInput(cardNumber, expiry, cvc) {
        this.setState({
            cardNumber: cardNumber,
            expiry: expiry,
            cvc: cvc,
        });
    }

    finishedBookingAttempt(success) {
        window.location.reload();
        if (success) {
            console.log("Booking Successful!");
            window.alert("Booking Successful!");
        } else {
            console.log("Booking Unsuccessful.");
            window.alert("Booking Unsuccessful.");
        }
    }

    attemptBooking(callback) {
        if (this.state.booking) { // already booking
            return;
        } else {
            this.setState({booking: true});
        }
        
        if (UserProfile.getData().id != "0jnIWg2Fl8N44zDT5sZXM1En2S23") {
            // not manager, check card info

            if (this.state.cardNumber.length != 16 || this.state.expiry.length != 4 || this.state.cvc.length != 3) {
                this.setState({
                    paymentErrorMessage: "Form not complete!",
                });
                callback(false);
            }
            this.setState({
                paymentErrorMessage: "",
            });

        } else {
            // manager, check seatmap, then cash payment
            if (!this.checkSeats()) {
                callback(false);
            }
        }

        var ticketCost = 0;
        var vipTicketsSorted = 0;
        var adultTicketsSorted = 0;
        var childTicketsSorted = 0;
        var _this = this; // strong reference to this so it doesn't get lost in callbacks

        const data = [];
        for (var seat of this.state.seatsSelected) {
            if (seat[0] == 4 || seat[0] == 5) {
                ticketCost = this.vipCost;
                vipTicketsSorted++;
            } else {
                if (adultTicketsSorted < parseInt(this.state.adultTickets)) {
                    ticketCost = this.adultCost;
                    adultTicketsSorted++;
                } else {
                    ticketCost = this.childCost;
                    childTicketsSorted++;
                }
            }
            data.push({
                movieID: this.movie.dictionary.movieID,
                showingID: this.state.selectedShowingID,
                seatID: seat[0] + ":" + seat[1],
                buyersName: this.state.name,
                email: this.state.email,
                ticketCost: ticketCost,
            });
        }
        var client = new Client("http://localhost:8080");
        var promises = [];
        for (var datum of data) {
            promises.push(new Promise((resolve, reject) => {
                client.requestDataFromEndpoint("/storeTicket", "get", datum, function (response) {
                    // this is where the state which determiens the OperationMessage component opacity
                    const responseCode = response.successfulOperation;
                    if (responseCode == true) {
                        resolve(1);
                    }
                    else if (responseCode == false) {
                        resolve(0);
                    }
                });
            }));
        }
        Promise.all(promises).then((data) => {
            for (var datum of data) {
                if (datum == 0) {
                    _this.setState({
                        successfulOp: false,
                    });
                    callback(false);
                }
            }
            _this.setState({
                successfulOp: true,
            });
            callback(true);
        });
    }

    submitForm() {
        // check form
        var isValid = this.isValidForm();
        if (!isValid) {
            return;
        }
        this.setState({
            toggleForm: Date.now(),
            toggleSeats: Date.now(),
        });
    }

    seatSelectionChange(newSelection) {
        this.setState({
            seatsSelected: newSelection,
        });
    }

    makeShowingButtons() {

        if (this.state.showings[this.state.showingDay] == null || this.state.showings[this.state.showingDay].length == 0) {
            return <div>No showings on this day!</div>
        }

        var result = [];
        var i = 0;

        var _this = this;

        for (var showing of this.state.showings[this.state.showingDay]) {
            var timeString = "";
            var date = new Date(showing["startTime"] * 1000);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            timeString = hours + ":" + minutes;
            result.push(<li key={i}><ShowingButtonComponent id={showing["showingID"]} timeString={timeString} clickHandler={this.pickShowing}/></li>);
            i++;
        }
        return result;
    }

    setActiveTab(id) {
        this.setState({
            showingDay: id,
        });
    }
 
    render() {

        var seatMap = <></>;
        if (this.state.selectedShowingSeatMap != undefined) {
            seatMap = <SeatMapComponent seats={this.state.selectedShowingSeatMap} selectionChange={this.seatSelectionChange}/>;
        }

        function finishedSeatMapButton(_this) {
            if (UserProfile.getData().id != "0jnIWg2Fl8N44zDT5sZXM1En2S23") {
                // not manager -> card payment
                return <div className="booking-progress-button" onClick={_this.goToPayment}>
                            Choose Payment Method
                            <img src={arrowIcon} alt=""/>
                        </div>;
            } else {
                // manager -> cash payment
                return <div className="booking-progress-button" onClick={() => {
                    _this.attemptBooking(_this.finishedBookingAttempt);
                }}>
                            Confirm Booking (Cash Payment)
                            <img src={arrowIcon} alt=""/>
                        </div>;
            }
        }

        return (
            <div className="wrapper">
                <div className="back-button-wrapper">
                <Link to={{
                pathname: "/whatson",
                state: []
                }}>
                    <button className="back-button">
                        Back
                    </button>
                </Link>
                </div>
                <div className="movie-detail-wrapper">
                    <div className="movie-header">
                        <h1> {this.movie.movieName} </h1>
                        <p className="movie-release-date"> Released {this.movie.dictionary.releaseDate} </p>
                    </div>
                    <div className="movie-content">
                        <div className="poster-wrapper">
                            <img src={this.movie.dictionary.posterURL} alt={this.props.name + " Poster"}/>
                        </div>
                        <div className="data-wrapper">
                            <p className="data-overview">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sem est, rutrum eu justo lacinia, mollis vulputate arcu. 
                                Nunc elementum porta ipsum, ut facilisis elit ornare vel. Sed elementum a risus sed vehicula. 
                                Donec dictum risus nisi, eget congue odio aliquet eget. Vestibulum eget nulla molestie, scelerisque massa quis, porta ipsum. 
                                Nunc egestas venenatis est tincidunt semper. Integer ornare nunc quis dictum lacinia. 
                                Nulla congue condimentum nisi non elementum. Aenean rhoncus porttitor turpis eu tincidunt.
                                In a neque sed quam pharetra fermentum. Fusce fermentum felis metus, eget imperdiet elit fermentum non. 
                                Mauris pharetra semper nisl, vitae vestibulum eros. Nam id libero augue. Donec ac sodales ligula, eu rutrum lorem. 

                                <br/><br/>
                                <b> Genres </b> : {this.displayGenres()} 
                            </p>
                        </div>
                    </div>

                    <div ref={this.scrollRef} className="ticket-booking-wrapper">
                        <h2> Book a Ticket </h2>
                        <hr/>
                        {this.bookingSummary()}
                        <SlideToggle toggleEvent={this.state.toggleShowing}>
                            {({ setCollapsibleElement}) => (
                                <div ref={setCollapsibleElement}>
                                    
                                    <DayTabComponent id={0} active={this.state.showingDay == 0} setActiveTab={this.setActiveTab}/>
                                    <DayTabComponent id={1} active={this.state.showingDay == 1} setActiveTab={this.setActiveTab}/>
                                    <DayTabComponent id={2} active={this.state.showingDay == 2} setActiveTab={this.setActiveTab}/>
                                    <DayTabComponent id={3} active={this.state.showingDay == 3} setActiveTab={this.setActiveTab}/>
                                    <DayTabComponent id={4} active={this.state.showingDay == 4} setActiveTab={this.setActiveTab}/>
                                    <DayTabComponent id={5} active={this.state.showingDay == 5} setActiveTab={this.setActiveTab}/>
                                    <DayTabComponent id={6} active={this.state.showingDay == 6} setActiveTab={this.setActiveTab}/>

                                    <ul className="booking-showing-buttons">
                                        {this.makeShowingButtons()}
                                    </ul>
                                </div>
                            )}
                        </SlideToggle>

                        <SlideToggle toggleEvent={this.state.toggleForm} collapsed>
                            
                            {({ setCollapsibleElement}) => (
                                <div ref={setCollapsibleElement}>
                                    <div className="ticket-booking-form-wrapper">
                                            <input onChange={(e) => this.updateInput("name", e)} type="text" id="name" name="name" placeholder="Full Name" autoComplete="off"/>&nbsp;&nbsp;
                                            <input onChange={(e) => this.updateInput("email", e)} type="text" id="email" name="email" placeholder="Email" autoComplete="off"/><br/><br/>
                                            Adult Tickets: <input onChange={(e) => this.updateInput("adultTickets", e)} type="number" 
                                            min="0" id="adultTickets" name="adultTickets" defaultValue={0}/><br/><br/>
                                            Child Tickets: <input onChange={(e) => this.updateInput("childTickets", e)} type="number" 
                                            min="0" id="childTickets" name="childTickets" defaultValue={0}/><br/><br/>
                                            VIP Tickets: <input onChange={(e) => this.updateInput("vipTickets", e)} type="number" 
                                            min="0" id="vipTickets" name="vipTickets" defaultValue={0}/><br/>
                                            {/** The movie id is read from the current props */}
                                            
                                            <div className="booking-form-error-message">
                                                {this.state.formErrorMessage}
                                            </div>

                                            <div className="booking-progress-button" onClick={this.submitForm}>
                                                Pick Seats
                                                <img src={arrowIcon} alt=""/>
                                            </div>
                                            { // This should be displayed after the booking submission has been sent
                                                <OperationMessage result={this.state.successfulOp} />
                                            }
                                    </div>
                                </div>
                            )}
                        </SlideToggle>
                        
                        <SlideToggle toggleEvent={this.state.toggleSeats} collapsed>
                            {({ setCollapsibleElement}) => (
                                <div ref={setCollapsibleElement}>
                                    {seatMap}
                                    <div className="booking-form-error-message">
                                        {this.state.seatsErrorMessage}
                                    </div>
                                    <div className="booking-progress-button-wrapper">
                                        {finishedSeatMapButton(this)}
                                    </div>
                                </div>
                            )}
                        </SlideToggle>
                        <SlideToggle toggleEvent={this.state.togglePayment} collapsed>
                            {({ setCollapsibleElement}) => (
                                <div ref={setCollapsibleElement}>
                                    <CardPaymentFormComponent updateState={this.takePaymentInput}/>
                                    <div className="booking-form-error-message">
                                        {this.state.paymentErrorMessage}
                                    </div>
                                    <div className="booking-progress-button-wrapper">
                                        <div className="booking-progress-button" onClick={() => {
                                            this.attemptBooking(this.finishedBookingAttempt);
                                        }}>
                                            Confirm Booking
                                            <img src={arrowIcon} alt=""/>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </SlideToggle>
                    </div>
                </div>
            </div>
        );
    }
 
}
 
export default MoviePage;
 
