/**
 * Imports -- modules, packages, assets
 */
 import React, { Component } from "reactn"; // should be imported in all components

 // import PaymentForm from "./Components/PaymentForm/PaymentForm";
 import PaymentForm from "./Components/PaymentForm/PaymentForm";

 import "./assets/PaymentDetailsPage.css";

 class PaymentDetailsPage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="wrapper">
                <div className="header">
                    <h2> Payment Details </h2>
                    <p> Save your payment details for faster checkout ! </p>
                    <hr/>
                </div>
                <div className="content">
                    <PaymentForm className="pform"></PaymentForm>
                </div>
            </div>
        );
    }

 }

 export default PaymentDetailsPage;