/*
 * contains all functionality for interacting with the TMDB API
*/

const fetch = require("node-fetch");

//returns the movies details. Searches the TMDB database using the IMDb ID
//returns the address used to make the query as well as the entire response
//returns a json
async function fetchByIMdB(apiKey, IMDbID, callback) {
    const APIaddress = "https://api.themoviedb.org/3/find/" + IMDbID + "?api_key=" + apiKey + "&language=en-US&external_source=imdb_id";
    var response = await fetch(APIaddress);
    var json = await response.json();
    var data;

    if (response.status == 200) {
        data = cleanData(json.movie_results[0]);
        callback({ ...{ "APIaddress": APIaddress }, ...data });
    }
    else if (response.status != 200) { console.log("Failed to fetch from TMDB") }
}

//returns the movies details. Searches the TMDB database useing the TMDB ID
//returns the address used to make the query as well as the entire response
//returns a json
async function fetchByID(apiKey, ID, callback) {
    const APIaddress = "https://api.themoviedb.org/3/movie/" + ID + "?api_key=" + apiKey + "&language=en-US";
    var response = await fetch(APIaddress);
    var json = await response.json();
    var data;
    
    if (response.status == 200) {
        data = cleanData(json);
        callback({ ...{ "APIaddress": APIaddress }, ...data });
    }
    else if (response.status != 200) { console.log("Failed to fetch from TMDB") }
}

//cleans the data returned by TMDB. Gets the title, overview, posterpath, release date, genres, languages & a flag for adult movies
//also returns a status value. 200 for a successfull clean
function cleanData(data) {
    if (data != null) {
        try {
            var posterPath = "https://www.themoviedb.org/t/p/w440_and_h660_face" + data.poster_path;
            var cleanedData = {
                'Status': 200, 'Title': data.title, 'Overview': data.Overview, 'PosterPath': posterPath,
                'ReleaseDate': data.release_date, 'Genres': data.genres, 'Languages': data.spoken_languages,
                'Adult': data.adult
            };

            return cleanedData;
        }
        catch (error) {
            console.error(error);
            return { 'Status': 404 };
        }
    }
    else {
        return { 'Status': 404 };
    }
}

module.exports = { fetchByIMdB, fetchByID, cleanData };