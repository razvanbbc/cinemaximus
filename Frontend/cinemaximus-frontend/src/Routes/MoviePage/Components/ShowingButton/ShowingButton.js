/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink,
  } from "react-router-dom";

import './showingbutton.css';

class ShowingButtonComponent extends React.Component {

    constructor(props) {
        super(props);

        this.clickHandler = this.clickHandler.bind(this);
    }

    clickHandler() {
        this.props.clickHandler(this.props.id, this.props.timeString);
    }

    render() {
        return (
            <button className="booking-showing-button" onClick={this.clickHandler}>
                {this.props.timeString}
            </button>
        );
    }

}

export default ShowingButtonComponent;
