/**
 * Imports -- modules, packages, assets
 */
import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import "./assets/Footer.css";

class FooterComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container fluid className="bg-primary fixed-bottom footer">
            <Row>
              <Col
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <p
                  style={{
                    textAlign: "center",
                    color: "#e1e8e7",
                    marginBottom: "0",
                    paddingBottom: "0",
                  }}
                >
                  {" "}
                  Copyright © 2021 Antonio, Leo, Magnus, Maddie, Adam{" "}
                </p>
              </Col>
            </Row>
          </Container>
        );
    }
}

export default FooterComponent;
