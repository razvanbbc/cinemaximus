/**
 * Imports -- modules, packages, assets
 */
import React, { Component } from "reactn"; // should be imported in all components
import {
    NavLink,
  } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";

import ReactLogo from "./assets/img/logo192.png";

import "./assets/Navbar.css"

import UserProfile from "../../Routes/UserArea/UserProfile";

import firebase from "../../Firebase";
import Authentification from "../../Routes/UserArea/Authentification";

class NavbarComponent extends React.Component { // component architecture

  // Component constructor
  constructor(props) {
    super(props);
    this.state = {
      links: [],
    }
  }

  // Builds the links list of the dropdown based on the user status (logged in, manager, etc)
  // returns the list with the dropdown items
  processLinks() {

    // get user status from local storage
    const userProfile = UserProfile.getData();
    var items = [];
    var keyCounter = 0;
    // set the dropdown contents
    if (userProfile === undefined || userProfile.id == "") { // user not logged in
      items.push(<p key={keyCounter++} className="dropdown-greeting"> Join us ! </p>);
      items.push(<NavDropdown.Divider key={keyCounter++}/>);
      items.push(<NavDropdown.Item key={keyCounter++} as={NavLink} to={{
        pathname: '/userarea',
        state: {
          action: 'signup',
        }
      }} eventKey="4.1">Sign Up</NavDropdown.Item>);
      items.push(<NavDropdown.Item key={keyCounter++} as={NavLink} to={{
        pathname: '/userarea',
        state: {
          action: 'login',
        }
      }} eventKey="4.2">Log In</NavDropdown.Item>);
      return items;
    }

    if (userProfile !== undefined && userProfile.id != "") { // logged in dropdown contents
      if (userProfile.id == "0jnIWg2Fl8N44zDT5sZXM1En2S23") { // manager logged in
        items.push(<p key={keyCounter++} className="dropdown-greeting"> Hello, boss ! </p>);
        items.push(<NavDropdown.Divider key={keyCounter++}/>);
        items.push(<NavDropdown.Item key={keyCounter++} eventKey="4.1">Account Details</NavDropdown.Item>);
        items.push(<NavDropdown.Item as={NavLink} to="/paymentDetails" key={keyCounter++} eventKey="4.2">Payment Details</NavDropdown.Item>);
        items.push(<NavDropdown.Divider key={keyCounter++}/>);
        items.push(<NavDropdown.Item key={keyCounter++} as={NavLink} to="/userarea" eventKey="4.3">Find Ticket</NavDropdown.Item>);
        items.push(<NavDropdown.Item key={keyCounter++} as={NavLink} to="/manager" eventKey="4.4">Manager</NavDropdown.Item>);
        items.push(<NavDropdown.Divider key={keyCounter++}/>);

        items.push(<NavDropdown.Item key={keyCounter++} eventKey="4.5" onClick={() => {
          var auth = new Authentification(firebase);
          auth.logout(function(response) {
              console.log(response);
              if (response == "SUCCESS") {
                  UserProfile.setData({
                      id: "",
                      name: "",
                  });
                  console.log("Successfully logged out!");
                  window.location.reload();
                  this.forceUpdate();
              } else {
                  console.error("Failed to log out!")
              }
          }.bind(this));
        }}>Logout</NavDropdown.Item>);
        return items;
      }
      else { // customer logged in
        items.push(<p className="dropdown-greeting"> Hello, {userProfile.name != "" ? userProfile.name : "customer"} ! </p>);
        items.push(<NavDropdown.Divider key={keyCounter++}/>);
        items.push(<NavDropdown.Item key={keyCounter++} eventKey="4.1">Account Details</NavDropdown.Item>);
        items.push(<NavDropdown.Item as={NavLink} to="/paymentDetails" key={keyCounter++} eventKey="4.2">Payment Details</NavDropdown.Item>);
        items.push(<NavDropdown.Divider key={keyCounter++}/>);
        items.push(<NavDropdown.Item key={keyCounter++} as={NavLink} to="/userarea" eventKey="4.3">Find Ticket</NavDropdown.Item>);
        items.push(<NavDropdown.Divider key={keyCounter++}/>);
        items.push(<NavDropdown.Item key={keyCounter++} as={NavLink} to="/userarea/logout" eventKey="4.4">Logout</NavDropdown.Item>);
        return items;
      }
    }

  }

  // Render component
  render() {

    return (
        <Navbar collapseOnSelect expand="lg" className="navbar" variant="dark">
        <Navbar.Brand href="#home" as={NavLink} to="/home">
        <img
          alt="logo"
          src={ReactLogo}
          width="30"
          height="30"
          className="d-inline-block align-top"
        />{" "}
          CineMaximus
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/home">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/whatson">What's on ?</Nav.Link>
          </Nav>
          <Nav>
            <NavDropdown title="User Area" id="nav-dropdown">
              {this.processLinks()}
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }

}

export default NavbarComponent;
