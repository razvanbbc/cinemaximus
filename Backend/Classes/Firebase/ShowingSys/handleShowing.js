/*
*    This class will handle showings interactions with the database
*/

require('firebase');
const showingClass = require("../../../Classes/showing");

class ShowingHandler {
	constructor(fbReference) {
		this.fbReference = fbReference;
		this.db = this.fbReference.database();
	}

	//adds a showing with the values passed to the database, generates a unique key for the showing instance
	//returns the ID of the showing in the database
	addShowing(screenNumber, startTime, seatMap, movieID, callback) {
		var thisShowing = new showingClass(screenNumber, startTime);
		var showingID = "-1";
		var ref = this.db.ref("Showings");

		if (typeof movieID == "string") {

			thisShowing.setSeatMap(seatMap);
			thisShowing.setMovieID(movieID);

			//Uniquely generated key to use as showingID
			showingID = ref.child("Showings").push().key;
			if (typeof showingID == "string" && showingID != "-1") {
				thisShowing.setShowingID(showingID);
				ref = this.db.ref("Showings/" + showingID);
				ref.set({
					'screenNumber': thisShowing.getScreenNumber(),
					'startTime': thisShowing.getStartTime(),
					'seatMap': thisShowing.getSeatMap(),
					'movieID': thisShowing.getMovieID()
				});
				callback(showingID);
			}
			else {
				console.log("failed to generate showingID in addShowing(screenNumber, startTime, seatMap, movieID, callback)");
				callback(false);
            }
		}
		else {
			console.log("movieID is not a string in addShowing(screenNumber, startTime, seatMap, movieID, callback)")
			callback(false);
        }
	}

	//takes a showing ID
	//returns the matching showing instance in the database
	getShowingByID(showingID, callback) {
		var ref;
		var data;
		var thisShowing = -1;

		if (typeof showingID == "string") { ref = this.db.ref("Showings/" + showingID) }
		else {
			console.log("showingID was not a string in getShowingByID(showingID, callback)");
			callback(false);
        }

		if (typeof showingID == "string") { ref = this.db.ref("Showings/" + showingID) }
		else {
			console.log("showingID was not a string in getShowingByID(showingID, callback)");
			callback(false);
        }

        ref.on('value', (snap) => {
            try {
                data = snap.val();
                thisShowing = new showingClass(data.screenNumber, data.startTime);
                thisShowing.setSeatMap(data.seatMap);
                thisShowing.setMovieID(data.movieID);
                thisShowing.setShowingID(snap.key);
                ref.off('value', undefined, this);
                callback(thisShowing);
            } catch {
                console.log("failed to get showing in getShowingByID(showingID, callback) ID: ", showingID);
                ref.off('value', undefined, this);
                callback(false);
            }
        });
    }

    //takes a start time and end time
    //returns all showing instances in that timeframe
    getShowingsByTime(startTime, endTime, callback) {
        var ref = this.db.ref("Showings");
        var showings;

        ref.orderByChild("startTime").startAfter(startTime - 1).endBefore(endTime + 1).on('value', (snap) => {
            try {
                showings = snap.val();
                ref.off('value', undefined, this);
                callback(showings);
            } catch {
                console.log("failed to get showings in getShowingsByTime(startTime, endTime, callback)");
                ref.off('value', undefined, this);
                callback(false);
            }
        });
    }
}

module.exports = ShowingHandler;
