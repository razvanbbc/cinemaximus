/*
*   This class will handle tickets interactions with the database
*/

require('Firebase');
const Ticket = require("../../../Classes/ticket");

class TicketHandler {
    constructor(fbReference) {
        this.fbReference = fbReference;
        this.db = this.fbReference.database();
        this.reserveSeat = this.reserveSeat.bind(this);
    }

    //adds a ticket with the values passed to the database, generates a unique key for the ticket instance
	//returns the ID of the ticket in the database
    addTicket(movieID, showingID, seatID, buyersName, email, price, callback) {
        var thisTicket = new Ticket(movieID, showingID, seatID, buyersName, email, price);
        var ticketID = -1;
        var ref = this.db.ref("Tickets");

        //Uniquely generate key to use as ticketID
        ticketID = ref.child("Tickets").push().key;
        if (typeof ticketID == "string" && ticketID != "-1") {
            this.reserveSeat(showingID, seatID);
            thisTicket.setTicketID(ticketID);
            ref = this.db.ref("Tickets/" + ticketID);
            ref.set({
                'movieID': thisTicket.getMovieID(),
                'showingID': thisTicket.getShowingID(),
                'seatID': thisTicket.getSeatID(),
                'buyersName': thisTicket.getBuyersName(),
                'email': thisTicket.getEmail(),
                'price': thisTicket.getPrice()
            });
            callback(ticketID);
        } else {
            console.log("failed to generate ticketID in addTicket(movieID, showingID, seatID, buyersName, email, price, callback)");
            callback(false);
        }
    }

    //takes a ticket ID
	//returns the matching ticket instance in the database
    getTicketByID(ticketID, callback) {
        var ref;
        var data;
        var thisTicket;

        if (typeof ticketID == "string") { ref = this.db.ref("Tickets/" + ticketID) }
        else {
            console.log("ticketID was not a string in getTicketByID(ticketID, callback)");
            callback(false);
        }

        ref.on('value', (snap) => {
            try {
                data = snap.val();
                thisTicket = new Ticket(data.movieID, data.showingID, data.seatID, data.buyersName, data.email);
                thisTicket.setTicketID(snap.key);
                ref.off('value', undefined, this);
                callback(thisTicket);
            } catch {
                console.log("failed to get ticket in getTicketByID(ticketID, callback)");
                ref.off('value', undefined, this);
                callback(false);
            }
        });
    }

    // Takes seatID and changes the value from false to true, to indicate seat is reserved
    reserveSeat(showingID, seatID) {

        let seat = seatID.split(":");
        let seatRow = seat[0];
        let seatSeat = seat[1];
        let ref = this.db.ref("Showings/" + showingID + "/seatMap/" + seatRow);
        ref.update({
            [seatSeat]: true
        })
    }
}

module.exports = TicketHandler;
