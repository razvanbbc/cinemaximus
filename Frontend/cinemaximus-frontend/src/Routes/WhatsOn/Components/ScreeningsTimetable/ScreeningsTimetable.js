/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink,
  } from "react-router-dom";

import './screeningstimetable.css'

import Client from "../../../../HTTP/Client/Client";
import DayTabComponent from './Components/DayTab/DayTab';
import DayScreeningsComponent from "./Components/DayScreenings/DayScreenings";
import RestResponse from "../../../../HTTP/Client/Response/Response";

class ScreeningsTimetableComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            day: 0,
            screenings: [],
        };
        this.setActiveTab = this.setActiveTab.bind(this);
        this.sortShowings = this.sortShowings.bind(this);
    }

    setActiveTab(id) {
        this.setState({
            day: id,
        });
    }

    componentDidMount() {
        this.getScreeningsInfo = this.getScreeningsInfo.bind(this);
        this.getScreeningsInfo();
    }

    // takes list of movie info from localstorage, sorts screenings by day
    sortShowings(data) {
        var showings = [[], [], [], [], [], [], []];

        var secondsPerDay = 3600 * 24;
        var startOfToday = new Date();
        startOfToday.setHours(0, 0, 0, 0);
        var now = Date.now();

        for (var movie of data) {
            for (var showing of movie["showingIDs"]) {
                var client = new Client("http://localhost:8080");
                new Promise((resolve, reject) => {
                    client.requestDataFromEndpoint("/getShowingByID", "get", {showingID: showing}, function(response) {
                        if (!response.data) {
                            resolve(-1);
                            return;
                        }
                        if (response.data["startTime"] == undefined || response.data["startTime"] == -1) {
                            resolve(-1);
                            return;
                        }
                        var startTime = new Date(response.data["startTime"] * 1000);
                        var diff = (startTime.getTime() - startOfToday.getTime()) / 1000;
                        var days = Math.floor(diff / secondsPerDay);
                        if (days < 0 || days > 6 || startTime < now) { // too far in future or in past
                            resolve(-1);
                            return;
                        }
                        resolve([days, response.data]);
                    });
                }).then((data) => {
                    if (data != -1) {
                        var day = data[0];
                        var details = data[1];
                        if (details["movieID"] in showings[day]) {
                            showings[day][details["movieID"]].push(details);
                        } else {
                            showings[day][details["movieID"]] = [details];
                        }
                    }
                });
            }
        }
        setTimeout(this.setState({screenings: showings}), 1000); // this maybe should be done better
    }

    getScreeningsInfo() {

        function checkForMovies() {
            let retrievedState = JSON.parse(localStorage.getItem("allMovies"));
            return retrievedState;
        }

        var _this = this;

        new Promise((resolve, reject) => {
            var retrievedState;
            var id = setInterval(function() {
                retrievedState = checkForMovies();
                if (retrievedState != null) {
                    clearInterval(id);
                    resolve(retrievedState);
                }
            }, 100);
        }).then((data) => {
            _this.sortShowings(data);
        });
    }

    render() {

        var content = null;

        // make sure initial render doesn't mess everything up
        if (this.state.films == -1) {
            content = "Loading";
        } else {
            content = <DayScreeningsComponent showings={this.state.screenings[this.state.day]}/>;
        }
        return (
            <div className="st-wrapper">
                <DayTabComponent id={0} active={this.state.day == 0} setActiveTab={this.setActiveTab}/>
                <DayTabComponent id={1} active={this.state.day == 1} setActiveTab={this.setActiveTab}/>
                <DayTabComponent id={2} active={this.state.day == 2} setActiveTab={this.setActiveTab}/>
                <DayTabComponent id={3} active={this.state.day == 3} setActiveTab={this.setActiveTab}/>
                <DayTabComponent id={4} active={this.state.day == 4} setActiveTab={this.setActiveTab}/>
                <DayTabComponent id={5} active={this.state.day == 5} setActiveTab={this.setActiveTab}/>
                <DayTabComponent id={6} active={this.state.day == 6} setActiveTab={this.setActiveTab}/>

                {content}
            </div>
        );
    }

}

export default ScreeningsTimetableComponent;
