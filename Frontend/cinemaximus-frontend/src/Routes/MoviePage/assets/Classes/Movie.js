/**
 * Movie logical structure for the frontend
 */
class Movie {

    constructor(movieID, movieName, releaseDate, posterURL, showingIDs, movieAPIID, movieGenres) {
        this.movieID = movieID;
        this.movieName = movieName;
        this.releaseDate = releaseDate;
        this.posterURL = posterURL;
        this.showingIDs = showingIDs;
        this.movieAPIID = movieAPIID;

        // set genres (received as dict of (id, string))
        var tempGenres = []
        for (var key in movieGenres) {
            tempGenres.push(movieGenres[key].name);
        }
        this.genres = tempGenres;
    }

    /** Getters */
    get dictionary() // returns the format below
    { 
        return {
            "movieID": this.movieID,
            "movieName": this.movieName,
            "releaseDate": this.releaseDate,
            "posterURL": this.posterURL,
            "showingIDs": this.showingIDs,
            "movieAPIID": this.movieAPIID,
            "movieGenres": this.genres
        };
    }

} 

export default Movie;