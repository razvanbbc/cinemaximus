"use strict"
/**
 * Class which handles emailing
 */

const nodemailer = require('nodemailer');

class Emailer {

    /** 
     * Constructor that creates the transporter configuration for the emailer
     * The provided constructor arguments are taken from the .env file
     */
    constructor(host, port, user, pass) {
        this.transporter = nodemailer.createTransport({
            host: host,
            port: port,
            secure: true, // use SSL
            auth: {
                user: `${user}`,
                pass: `${pass}`,
            },
        });
    }

    /**
     * Sends an email using the data in body - email, ticket
     * @param {Dict} body 
     * @param {function(success, err)} callback 
     */
    async sendEmailWithTicket(body, callback) {
        // create email configuration
        const config = {
            from: '"cineMaximus" <sepcinema21@gmail.com>',     
            to: body.email,                                    
            subject: `Your Ticket With ID - ${body.ticket.getTicketID()}`,  
            text: "Hello World",                     
            html: this.getHTMLTicket(body.ticket)          
        }
        await this.transporter.sendMail(config, (err) => {
            // callback after sendEmail()
            if (!err) { // sendEmail() success
                callback(true, undefined);
            }
            else { // sendEmail() fail
                callback(false, err);
            }
        });
    }

    /**
     * Returns the string representation of a ticket object in HTML
     * for emailing purposes
     * @param {Ticket} ticket 
     */
    getHTMLTicket(ticket) {
        var html = "";
        html += "<b> Thank you for booking a ticket at cineMaximus ! </b> <br/>";
        html += "<hr/>";
        html += "<b>Your Ticket</b><br/><br/>";
        html += `Name : ${ticket.getBuyersName()}<br/>`;
        html += `Movie : ${ticket.getMovieID()}<br/>`; // get movie by id
        html += `Seat : ${ticket.getSeatID()}<br/>`;
        html += `Ticket ID : ${ticket.getTicketID()}<br/>`;
        html += "<hr/>";
        html += "<i> - the cineMaximus team </i>&#11088;";

        return html;
    }

}

module.exports = Emailer;