/*
*    This class will handle user interactions with the database
*/

require('firebase');
const userClass = require("../../../Classes/user");

class UserHandler {
    constructor(fbReference) {
        this.fbReference = fbReference;
        this.db = this.fbReference.database();
    }

    //adds a user with the values passed to the database, generates a unique key for the movie instance
    //returns the ID of the user in the database
    addUser(name, email, phoneNo, ticketIDs, callback) {
        var thisUser = new userClass(name, email, phoneNo, ticketIDs);
        var userID = -1;
        var ref = this.db.ref("Users");

        thisUser.setTicketIDs(ticketIDs);

        //Uniquely generated key to use as userID
        userID = ref.child("Users").push().key;
        if (typeof userID == "string" && userID != "-1") {
            thisUser.setUserID(userID);
            ref = this.db.ref("Users/" + userID);
            ref.set({
                'name': thisUser.getName(),
                'email': thisUser.getEmail(),
                'phoneNo': thisUser.getPhoneNo(),
                'ticketIDs': thisUser.getTicketIDs()
            });
            callback(userID);
        } else {
            console.log("failed to generate userID in addUser(name, email, phoneNo, ticketIDs, callback)");
            callback(false);
        }
    }

    //takes a userID
    //returns the matching user instance in the database
    getUserByID(userID, callback) {
        var ref;
        var data;
        var thisUser;

        if (typeof userID == "string") { ref = this.db.ref("Users/" + userID) }
        else {
            console.log("userID was not a string in getUserByID(userID, callback)");
            callback(false);
        }

        ref.on('value', (snap) => {
            try {
                data = snap.val();
                thisUser = new userClass(data.name, data.email, data.phoneNo);
                thisUser.setTicketIDs = data.ticketIDs
                thisUser.setUserID(snap.key);
                ref.off('value', undefined, this);
                callback(thisUser);
            } catch {
                console.log("failed to get user in getUserByID(userID, callback)");
                ref.off('value', undefined, this);
                callback(false);
            }
        });
    }
}

module.exports = UserHandler;