/**
 * Imports -- modules, packages, assets
 */
import React from "react";

import './button.css'

class ButtonComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <a onClick={(e) => this.props.clickHandler(e, this.props.name)}>
                <div className={this.props.class}>{this.props.label}</div>
            </a>
        );
    }
}

export default ButtonComponent;