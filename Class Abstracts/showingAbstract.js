/*
 * abstract for the showing class implementations in backend and frontend
*/

/*
 * {
 *      int: showingID          (primary key)
 *      int: screenNumber       (number of the screen where the showing will take place)
 *      dateTime: startTime     (start time of the showing)
 *      bool[][]: seatMap       (boolean array indicating if a given seat is already occupied)
 * }
*/

class showingAbstract
{
    constructor(showingID, screenNumber, startTime)
    {
        if (this.constructor == showingAbstract)
        {
            throw new Error("Cannot instanciate abstract class");
        }

        this.showingID = showingID;
        this.screenNumber = screenNumber;
        this.startTime = startTime;
        this.seatMap = this.initialiseSeats(10, 10);
    }

    /** Getters */
    get dictionary() // returns the format below
    {
        return {
            "showingID": this.showingID,
            "screenNumber": this.screenNumber,
            "startTime": this.startTime,
            "seatArray": this.seatArray
        };
    }

    //initialises the seatmap to false
    initialiseSeats(rows, seatsPerRow)
    {
        var rowArray = [], seatMap = [];

        for (var i = 0; i < seatsPerRow; i++) 
            rowArray.push(false);
        for (var i = 0; i < rows; i++)
            seatMap.push(rowArray);
        return seatMap;
    }
}

class Seat {
    constructor(occupied, VIP) {
        if (typeof occupied == "boolean" && typeof VIP == "boolean") {
            this.occupied = occupied;
            this.VIP = VIP;
        }
        else if (typeof occupied == "boolean" && typeof VIP != "boolean") {
            this.occupied = occupied;
            this.VIP = false;
        }
        else if (typeof occupied != "boolean" && typeof VIP == "boolean") {
            this.occupied = false;
            this.VIP = VIP;
        }
        else {
            this.occupied = false;
            this.VIP = false;
        }
    }

    setOccupied(occupied) {
        if (typeof occupied == "boolean") { this.occupied = occupied }
        else { console.log("occupied must be a boolean") }
    }

    setVIP(VIP) {
        if (typeof VIP == "boolean") { this.VIP = VIP }
        else { console.log("VIP must be a boolean") }
    }

    getOccupied() { return this.occupied }

    getVIP() { return this.occupied }
}

module.exports = showingAbstract;
