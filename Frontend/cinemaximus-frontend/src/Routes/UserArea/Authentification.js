/*
    The class which handles authentification (login and signup) using Firebase Authentification
 */

class Authentification {

    constructor(fbReference) {
        this.fb = fbReference;
    }

    signUp(name, email, password, callback) {
        this.fb.auth().createUserWithEmailAndPassword(email, password)
            .then((userCredential) => {
                let user = userCredential.user;
                user.updateProfile({
                    displayName: name
                })
                    .then(function() {
                        //Successfully updated  :)
                        callback([user, undefined]);
                    }).catch(function(error) {
                        let errorCode = error.code;
                        let errorMessage = error.message;
                        callback([user, {errorCode: errorCode, errorMessage: errorMessage}]);
                });
            })
            .catch((error) => {
                let errorCode = error.code;
                let errorMessage = error.message;
                callback([undefined, {errorCode: errorCode, errorMessage: errorMessage}]);
            })
    }

    login(email, password, callback) {
        this.fb.auth().signInWithEmailAndPassword(email, password)
            .then((userCredential) => {
                let user = userCredential.user;
                callback([user, undefined]);
            })
            .catch((error) => {
                let errorCode = error.code;
                let errorMessage = error.message;
                callback([undefined, {errorCode: errorCode, errorMessage: errorMessage}]);
            })
    }

    logout(callback) {
        this.fb.auth().signOut()
            .then(() => {
                callback("SUCCESS");
            })
            .catch((error) => {
                callback(error);
            })
    }

}

export default Authentification;
