/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink,
  } from "react-router-dom";

import './assets/WhatsOn.css';
import upArrow from './assets/up-arrow.png';
import chevrons from './assets/chevrons.png';
import FilmLinksAreaComponent from "./Components/FilmLinksArea/FilmLinksArea";
import ScreeningsTimetableComponent from "./Components/ScreeningsTimetable/ScreeningsTimetable";
import FilmSearcherComponent from "./Components/FilmSearcher/FilmSearcher";

import SlideToggle from "react-slide-toggle";
import Client from "../../HTTP/Client/Client";
import FilmLinkComponent from "./Components/FilmLinksArea/Components/FilmLink/FilmLink";

class WhatsOn extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            toggleSRA: 0,
            sidebarState: "sb-closed",
            searchResults: [],
            sraIsOpen: false,
        };

        this.toggleSRA = this.toggleSRA.bind(this);
        this.toggleSidebar = this.toggleSidebar.bind(this);
        this.doubleToggleSRA = this.doubleToggleSRA.bind(this);
        this.searchQuery = this.searchQuery.bind(this);
    }

    // SRA = search results area
    toggleSRA() {
        this.setState({
            toggleSRA: Date.now(),
            sraIsOpen: !this.state.sraIsOpen,
        });
    }

    toggleSidebar() {
        if (this.state.sidebarState == "sb-open") {
            this.setState({
                sidebarState: "sb-closed",
            });
        } else {
            this.setState({
                sidebarState: "sb-open",
            });
        }
    }

    doubleToggleSRA(query) {
        console.log("double toggling");
        this.setState({
            toggleSRA: Date.now(),
            sraIsOpen: !this.state.sraIsOpen,
        });
        var _this = this;
        setTimeout(() => {
            this.searchQuery(query);
        }, 500);
    }

    searchQuery(query) {

        if (this.state.sraIsOpen) {
            this.doubleToggleSRA(query);
            return;
        }

        var keywords = query.replace(" ", ",");

        var client = new Client("http://localhost:8080");
        new Promise((resolve, reject) => {
            client.requestDataFromEndpoint("/getMoviesByKeywords", "get", {keywords: keywords}, function(response) {
                console.log(response);
                resolve(response.data);
            });
        }).then((data) => {
            if (typeof data == "boolean") {
                return;
            }
            if (data.length == 0) {
                this.setState({
                    searchResults: [],
                });
                this.toggleSRA();
                return;
            }
            var allMovies = JSON.parse(localStorage.getItem("allMovies"));
            var results = [];
            for (var movie of data) {
                var id = movie.movieID;
                for (var m of allMovies) {
                    if (m.id == id) {
                        results.push(m);
                    }
                }
            }
            this.setState({
                searchResults: results,
            });
            this.toggleSRA();
        });
        
    }

    render() {
        
        var results = this.state.searchResults;
        var formattedSearchResults = [];
        var i = 0;
        for (var result of results) {
            formattedSearchResults.push(
                <FilmLinkComponent key={i++} details={result}/>
            );
        }
        var sraClass = " sra-with-results";

        if (formattedSearchResults.length == 0) {
            formattedSearchResults = <div className="sra-no-results-message">No results found!</div>;
            sraClass = " sra-no-results";
        }


        return (
            <div className="whatson-page-wrapper">
                <div className={"whatson-sidebar " + this.state.sidebarState}>
                    <div className="sidebar-title">Showing this Week</div>
                    <div className="sidebar-st-wrapper">
                        <ScreeningsTimetableComponent />
                    </div>
                </div>
                <div className="toggle-sidebar-button" onClick={this.toggleSidebar}>
                    <div className="toggle-sidebar-button-label-wrapper" >
                        <span className="toggle-sidebar-button-label">TIMETABLE</span>
                    </div>
                </div>
                <div className="whatson-main-wrapper">
                    <div className="whatson-header-wrapper">
                        <div className="whatson-search-wrapper">
                            <FilmSearcherComponent searchQuery={this.searchQuery}/>
                            <SlideToggle toggleEvent={this.state.toggleSRA} collapsed>
                                {({ setCollapsibleElement}) => (
                                    <div ref={setCollapsibleElement} className={"search-result-area" + sraClass}>
                                        {formattedSearchResults}
                                        <button className="close-sra-button" onClick={this.toggleSRA}>
                                            <img src={chevrons} alt=""/>
                                        </button>
                                    </div>
                                )}
                            </SlideToggle>
                        </div>
                    </div>
                    <div className="whatson-content-wrapper">
                        <div className="whatson-main-components-wrapper">
                            <div className="whatson-films-wrapper">
                                <div className="whatson-comp-wrapper">
                                    <div className="whatson-comp-title">Films</div>
                                    <FilmLinksAreaComponent />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default WhatsOn;
