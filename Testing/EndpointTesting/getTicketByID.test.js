/********************* Requirements **********************/
const fetch = require("node-fetch");

var movieID = null;
var showingID = null;
var ticketID = null;

beforeAll(async () => {
    var res, json, endpointAddress;

    endpointAddress = "http://localhost:8080/addMovie?movieName=ticketTestMovie&showingIDs=[]&movieAPIID=-1"
    res = await fetch(endpointAddress);
    json = await res.json();

    movieID = json.data.MovieID;

    endpointAddress = "http://localhost:8080/addShowing?movieID=" + movieID + "&screenNumber=-2&startTime=0";
    res = await fetch(endpointAddress);
    json = await res.json();

    showingID = json.data;

    endpointAddress = "http://localhost:8080/storeTicket?movieID=" + movieID + "&showingID=" + showingID + "&seatID=0:0&buyersName=testBuyer&email=sepcinema21@gmail.com";
    res = await fetch(endpointAddress);
    json = await res.json();

    ticketID = json.data;    
});

//Tests the getMovieTMDB endpoint
it("Storing a Ticket", async () => {
    var endpointAddress = "http://localhost:8080/getTicketByID?ticketID=" + ticketID;
    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data.ticket.ticketID).toEqual(ticketID);
});