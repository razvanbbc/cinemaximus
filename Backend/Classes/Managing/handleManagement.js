/*
*   This class will handle the business owner's backlog items
*/

require('Firebase');
const handleShowings = require('../Firebase/ShowingSys/handleShowing');
const handleMovies = require('../Firebase/MovieSys/handleMovie');

class ManagementHandler {
    constructor(fbReference)
    {
        this.fbReference = fbReference;
        this.db = this.fbReference.database();
    }

    //get the number of tickets sold for each movie with a showing between startdate and enddate
    //returns a dictionary of moviename:ticketsales
    calcTicketCount(startDate, endDate, callback)
    {
        var ref = this.db.ref("Tickets/");
        var dict = {};
        var showingID;
        var showing = undefined;
        var movieID;
        var countsPerMovie = {};
        var movie;
        var showingHandler = new handleShowings(this.fbReference);
        var movieHandler = new handleMovies(this.fbReference);
        var tickets;
        var vtCount = 0; //holds a count of the tickets sold in the timespan
        var tCount = 0; //counter for the number of total tickets currently looked tru
        var len = 0; //total number of tickets retreived from database
        var cb = true; //whether we should trigger the callback or not (stops it being called multiple times)

        //get the data from the db
        ref.get().then( function (snap) {
            //store the data in tickets and get the length
            tickets = snap.val();
            len = Object.keys(tickets).length;

            //iterate over all the tickets
            for (let m in tickets){
                //add 1 to the count of the total number of tickets
                tCount++;

                //get the showing ID for the ticket
                showingID = tickets[m].showingID;

                //if the showing ID is not valid, skip this ticket
                if (showingID == -1) { continue; }

                //if valid, get the showing data from db
                showingHandler.getShowingByID(showingID, function (data) {
                    showing = data;

                    //check showing ID is within the timespan given
                    if( showing.startTime >= startDate && showing.startTime <= endDate )
                    {
                        //if it is, increase the counter for it
                        vtCount++;

                        //get the movie ID from the ticket
                        movieID = showing.movieID;

                        //check if movie ID is in dict
                        if(!(movieID in dict))
                        {
                            //add it and initialise count to 0 if not
                            dict[movieID] = 0;
                        }
                        //add 1 to the count;
                        dict[movieID]++;

                        //then we need to convert the IDs into names for every dict entry
                        for (let m in dict)
                        {
                            //get the movie object from the db bd the ID
                            movieHandler.getMovieByID(m, function ( data ) {
                                movie = data;
                                //transfer the counts and the name into a new dictionary
                                countsPerMovie[movie.getMovieName()] = dict[m];

                                //now we need to count the counts in countsPerMovie
                                //we need to check it is the same as vtCount
                                //if not, we haven't finished getting all the tickets.
                                var fcount = 0;
                                for (let m in countsPerMovie)
                                {
                                    fcount += countsPerMovie[m];
                                }

                                //only call the callback if:
                                    // 1. we haven't already called it
                                    // 2. we have all the movies in the countsPerMovie dict
                                    // 3. we have got all the tickets from dict
                                    // 4. we have looked through all the tickets we got from the db
                                if(cb && fcount == vtCount && tCount == len)
                                {
                                    //once we have triggered the callback once, we don't want to do it again, so set this flag to false
                                    cb = false;
                                    //callback
                                    callback(countsPerMovie);
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    //returns the revenue generated for each movie between startdate and enddate
    //returns a dictionary of moviename:income
    calcTicketIncome(startDate, endDate, callback)
    {   
        // a function called further down to update the dictionary values for the incomes
        function updateDicts(price, showingID)
        {
            //if valid, get the showing data from db
            showingHandler.getShowingByID(showingID, function (data) {
                if (data != false) {
                    showing = data;

                    //check showing ID is within the timespan given
                    if( showing.startTime >= startDate && showing.startTime <= endDate )
                    {
                        console.log("st:" + showing.startTime);
                        var st = showing.startTime;
                        //st = number(st);
                        //if it is, increase the price counter for it
                        vtCount += price;

                        //add the ticket price to the countsPerWeek dictionary
                        for (let w in countsPerWeek)
                        {
                            var e = parseInt(w) + 608400;
                            if (st >= w && st < e)
                            {
                                countsPerWeek[w] += price;
                            }
                        }

                        //get the movie ID from the ticket
                        movieID = showing.movieID;

                        //check if movie ID is in dict
                        if(!(movieID in dict))
                        {
                            //add it and initialise count to 0 if not
                            dict[movieID] = 0;
                        }
                        //add the price to the count;
                        dict[movieID] += price;

                        //then we need to convert the IDs into names for every dict entry
                        for (let m in dict)
                        {
                            //get the movie object from the db bd the ID
                            movieHandler.getMovieByID(m, function ( data ) {
                                movie = data;

                                if (data != false)
                                {
                                    //transfer the incomes and the name into a new dictionary
                                    countsPerMovie[movie.movieName] = dict[m];

                                    //now we need to count the counts in countsPerMovie
                                    //we need to check it is the same as vtCount
                                    //if not, we haven't finished getting all the incomes.
                                    var fcount = 0;
                                    for (let m in countsPerMovie)
                                    {
                                        fcount += countsPerMovie[m];
                                    }
                                    //only call the callback if:
                                        // 1. we haven't already called it
                                        // 2. we have all the movies in the countsPerMovie dict
                                        // 3. we have got all the incomes from dict
                                        // 4. we have looked through all the tickets we got from the db
                                    //console.log("vtCount: " + vtCount + "fCount: " + fCount);
                                    if(cb && fcount == vtCount && tCount == len)
                                    {
                                        //once we have triggered the callback once, we don't want to do it again, so set this flag to false
                                        cb = false;
                                        //callback
                                        callback("none", countsPerMovie, countsPerWeek, vtCount);
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }

        var ref = this.db.ref("Tickets/");
        var dict = {};
        var showing = undefined;
        var movieID;
        var countsPerMovie = {};
        var countsPerWeek = {};
        var movie = undefined;
        var showingHandler = new handleShowings(this.fbReference);
        var movieHandler = new handleMovies(this.fbReference);
        var tickets;
        var vtCount = 0; //holds a count of the incomes generated in the timespan
        var tCount = 0; //counter for the number of total tickets currently looked tru
        var len = 0; //total number of tickets retreived from database
        var cb = true; //whether we should trigger the callback or not (stops it being called multiple times)

        //add week interval keys to the week dictionary
        var w = startDate;
        while (w < endDate)
        {
            countsPerWeek[w] = 0;
            w += 608400;
        }
        //get the data from the db
        ref.get().then( function (snap) {
            //store the data in tickets and get the length
            tickets = snap.val();
            len = Object.keys(tickets).length;

            //iterate over all the tickets
            for (let m in tickets){
                //add 1 to the count of the total number of tickets
                tCount++;

                //console.log("ShowingID: " + tickets[m].showingID + ", price: " + tickets[m].price);

                //update the dictionaries with this ticket's data if it has a valid showing ID
                if (tickets[m].showingID != -1) { updateDicts(tickets[m].price, tickets[m].showingID); }
            }
        }).catch(function (err) {
            // console.log(err);
            callback(err, undefined, undefined, undefined);
        });

    }

}

module.exports = ManagementHandler;
