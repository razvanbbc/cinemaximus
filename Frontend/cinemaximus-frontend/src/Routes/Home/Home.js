/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components
import {
    NavLink,
  } from "react-router-dom";

import './assets/Home.css';
import CuteCinemaPic from './assets/cinemapic.png';

class Home extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="wrapper-home">
                <div className="content-home">
                    <div className="centered-div">
                        <h1> Welcome to CineMaximus ! </h1>
                    </div>
                </div>
                <div className="visual-home-container">
                    <div className="visual-home">
                        {/** Credits : https://www.odeon.co.uk/films/ */}
                        <img src={CuteCinemaPic} className="cute-cinema-pic" />
                    </div>
                    <div className="text-home">
                        <h6>
                            Escape into film and support your local independent cinema at Cinemaximus. We show both big budget blockbusters and stunning indie films over three screens tailored to maximise your cinema experience. Treat yourself with a VIP seats in all out screens, boasting a wider chair, more leg room, and a perfect view of the screen.
                        </h6>
                    </div>
                </div>
                <div className="schedule-container">
                    Open Monday - Friday from 10:00AM to 23:00PM
                    </div>
                </div>
        );
    }

}

export default Home;
