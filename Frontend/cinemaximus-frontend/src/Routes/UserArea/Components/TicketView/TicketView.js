import React, { Component } from "reactn";
import "./TicketView.css"
import html2canvas from "html2canvas";
import jsPDF from "jspdf";

import Client from "../../../../HTTP/Client/Client";

class TicketView extends React.Component {

    constructor(props) {
        super(props);
        this.printTicket = this.printTicket.bind(this);
        this.state = {
            movieName: null,
            startTime: null
        }
    }

    componentWillReceiveProps(newProps) {
        if(newProps.data !== this.props.data) {
            this.getMovieName(newProps.data.ticket.movieID);
            this.getShowingTime(newProps.data.ticket.showingID);
        }
    }

    /**
     * Generates the PDF of the ticket component and
     * makes it downloadable
     */
    printTicket() {
        const input = document.getElementById("ticket-to-print");
        const pdf = new jsPDF({ unit: "px", format: "letter", userUnit: "px" });
        pdf.setFontSize(15);
        pdf.html(input, { html2canvas: { scale: 0.90 } }).then(() => {
            pdf.save("ticket.pdf");
        });
    }

    /**
     * Gets the movie name from the DB using a movie ID
     * @param {string} movieID 
     */
    getMovieName(movieID) {
        var _this = this;
        const client = new Client("http://localhost:8080");
        client.requestDataFromEndpoint("/getMovieByID", "get", {movieID: movieID}, function (response) {
            _this.setState({
                movieName: response.data.Title
            });
        });
    }

    /**
     * Gets the showing time using showingID
     * @param {string} showingID 
     */
    getShowingTime(showingID) {
        var _this = this;
        const client = new Client("http://localhost:8080");
        client.requestDataFromEndpoint("/getShowingByID", "get", {showingID: showingID}, function (response) {
            var formattedTimeString = "";
            var date = new Date(response.data.startTime * 1000);
            var hours = date.getHours();
            var minutes = date.getMinutes();
            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            formattedTimeString = hours + ":" + minutes;
            _this.setState({
                startTime: formattedTimeString
            });
        });
    }

    render() {
        if (this.props.data !== undefined) {
            return (
                <div className="ticket-all">
                    <div className="ticket-wrapper">
                        <div id="ticket-to-print" className="ticket">
                            <div className="ticket-qr-code">
                                {/** QR CODE IMAGE HERE **/}
                                <img src={"data:image/png;base64," +  this.props.data.qrcode} />
                            </div>
                            <div className="ticket-header">
                                {this.props.data.ticket.buyersName}
                            </div>
                            <hr/>
                            <div className="ticket-screening-wrapper">
                                <div className="showing-wrapper">
                                    Movie <br/>
                                    {this.state.movieName}
                                </div>
                                <div className="seating-wrapper">
                                    Time <br/>
                                    {this.state.startTime}
                                </div>
                                <div className="seating-wrapper">
                                    Seats <br/>
                                    {this.props.data.ticket.seatID}
                                </div>
                            </div>
                            <hr/>
                            <div className="ticket-email-wrapper">
                                {this.props.data.ticket.email}
                            </div>
                            <hr/>
                            <div className="ticket-footer-wrapper">
                                <div className="ticket-footer">
                                    {this.props.data.ticket.ticketID}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="button-wrapper">
                        <button onClick={this.printTicket} className="pdf-button"> Get Printable Ticket </button>
                    </div>
                </div>
            );
        } 
        else {
            return null;
        }
    }

}

export default TicketView;