/********************* Requirements **********************/
const fetch = require("node-fetch");

var movieID = null;

beforeAll(async () => {
    var res, json, endpointAddress;

    endpointAddress = "http://localhost:8080/addMovie?movieName=movieTestMovie&showingIDs=[]&movieAPIID=-1"
    res = await fetch(endpointAddress);
    json = await res.json();

    movieID = json.data.MovieID;
});

//Tests the getMovieTMDB endpoint
it("Getting a Movie by ID", async () => {
    var endpointAddress = "http://localhost:8080/getMovieByID?movieID=" + movieID;

    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data.movieName).toEqual('movieTestMovie');
});