/*
 * concrete implementation of the showing class for backend
*/

/*
 * {
 *      string: showingID       (primary key)
 *      int: screenNumber       (number of the screen where the showing will take place)
 *      int: startTime          (start time of the showing in unix time)
 *      bool[][]: seatMap       (boolean array indicating if a given seat is already occupied)
 *      string: movieID         
 * }
*/

const Abstract = require("../../Class Abstracts/showingAbstract");

class Showing extends Abstract {
    constructor(screenNumber, startTime, seatMapW = 10, seatMapH = 10, movieID = -1) {
        var holdSN, holdST;
        if (Number.isInteger(screenNumber) == true) { holdSN = screenNumber }
        else { holdSN = -1 }

        if (Number.isInteger(startTime) == true) { holdST = startTime }
        else { holdST = -1 }

        super(0, holdSN, holdST);

        if (Number.isInteger(seatMapW) == true && Number.isInteger(seatMapH) == true) { this.setSeatMap(this.initialiseSeats(seatMapW, seatMapH)) }
        else if (Number.isInteger(seatMapW) == true && Number.isInteger(seatMapH) == false) { this.setSeatMap(this.initialiseSeats(seatMapW, 10)) }
        else if (Number.isInteger(seatMapW) == false && Number.isInteger(seatMapH) == true) { this.setSeatMap(this.initialiseSeats(10, seatMapH)) }
        else (this.setSeatMap(this.initialiseSeats(10, 10)))

        if (typeof movieID == "string") { this.movieID = movieID }
        else { this.movieID = "-1" }
    }

    setShowingID(showingID) {
        if (typeof showingID == "string") { this.showingID = showingID }
        else { console.log("Cannot set showingID as null in setShowingID(showingID)") }
    }

    setScreenNumber(screenNumber) {
        if (Number.isInteger(screenNumber) == true) { this.screenNumber = screenNumber }
        else { console.log("screenNumber must be an integer in setScreenNumber(screenNumber)") }
    }

    setStartTime(startTime) {
        if (Number.isInteger(startTime) == true) { this.startTime = startTime }
        else { console.log("startTime must be a integer in setStartTime(startTime)") }
    }

    setSeatMap(seatMap) {
        if (Array.isArray(seatMap) == true && Array.isArray(seatMap[0]) == true) { this.seatMap = seatMap }
        else { console.log("seatMap must be a 2d array in setSeatMap(seatMap)") }
    }

    setMovieID(movieID) {
        if (typeof movieID == "string") { this.movieID = movieID }
        else { console.log("movieID must be a string in setMovieID(movieID)") }
    }

    reinitialiseSeatMap(seatMapW, seatMapH) {
        if (Number.isInteger(seatMapW) == true && Number.isInteger(seatMapH) == true) { this.setSeatMap(this.initialiseSeats(seatMapW, seatMapH)) }
        else { console.log("seatMapW and seatMapH must be integers") }
    }

    getShowingID() { return this.showingID }

    getScreenNumber() { return this.screenNumber }

    getStartTime() { return this.startTime }

    getSeatMap() { return this.seatMap }

    getMovieID() { return this.movieID }
}

module.exports = Showing;