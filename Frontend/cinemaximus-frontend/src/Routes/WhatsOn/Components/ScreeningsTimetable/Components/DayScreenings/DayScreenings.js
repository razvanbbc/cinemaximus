/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink,
  } from "react-router-dom";

import './dayscreenings.css'

import FilmScreeningsComponent from './Components/FilmScreenings/FilmScreenings'

class DayScreeningsComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        var fs = [];
        for (var movieID in this.props.showings) {
            var allMovies = JSON.parse(localStorage.getItem("allMovies"));
            let movieName = "";
            for (var deet of allMovies) {
                if (deet["id"] == movieID) {
                    movieName = deet["movieName"];
                    break;
                }
            }
            if (movieName != "") {
                fs.push(<FilmScreeningsComponent key={movieID} id={movieID} name={movieName} times={this.props.showings[movieID]}/>);
            }
        }

        if (fs.length == 0) { // no films showing today :(

            return (
                <div className="day-screenings-wrapper">
                    Sorry, we aren't showing any films today!
                </div>
            );
        }

        return (
            <div className="day-screenings-wrapper">
                {fs}
            </div>
        );
    }

}

export default DayScreeningsComponent;
