/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink,
  } from "react-router-dom";

import './assets/filmsearcher.css';
import searchIcon from './assets/magnifying-glass.png';

class FilmSearcherComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            query: "",
        };

        this.handleInput = this.handleInput.bind(this);
        this.submitQuery = this.submitQuery.bind(this);
    }

    handleInput(e) {
        e.preventDefault();
        if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 64 && e.keyCode < 91) || e.keyCode == 32) {
            let newString = this.state.query + e.key;
            e.target.value = newString.toUpperCase();
            this.setState({
                query: newString.toLowerCase(),
            });
        } else if (e.key == "Backspace") {
            let l = this.state.query.length;
            if (l > 0) {
                let newString = this.state.query.substring(0, l - 1);
                e.target.value = newString.toUpperCase();
                this.setState({
                    query: newString,
                });
            }
        } else if (e.key == "Enter") {
            this.submitQuery();
        }
    }

    submitQuery() {
        if (this.state.query != "") {
            this.props.searchQuery(this.state.query);
        }
    }

    render() {
        return (
            <div className="film-searcher-wrapper">
                <button onClick={this.submitQuery}>
                    <img src={searchIcon} alt=""/>
                </button>
                <input type="text" onKeyDown={this.handleInput} />
            </div>
        );
    }

}

export default FilmSearcherComponent;
