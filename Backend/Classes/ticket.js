/*
 * concrete implementation of ticket class for backend
*/

/*
 * {
 *      string: ticketID           (primary key)
 *      *string: movieID           (ID of the movie the ticket is for)
 *      *string: showingID         (ID of the showing which the ticket is for)
 *      string: seatID          (ID of the seat should contain two integers seperated by a 'x:y')
 *      string: buyersName      (name of the buyer of the ticket)
 *      string: email           (email the ticket is sent to)
 *      number: price              (the cost of the ticket in pounds)
 * }
*/

const Abstract = require("../../Class Abstracts/ticketAbstract");

class Ticket extends Abstract {
    // Constructor adds default ticketID of 0, ticketID handled when ticket is stored
    constructor(movieID, showingID, seatID, buyersName, email, price) {
        var holdMID, holdShID, holdSeID, holdBN, holdE, holdP;
        if (typeof movieID == "string") { holdMID = movieID }
        else { holdMID = "-1" }

        if (typeof showingID == "string") { holdShID = showingID }
        else { holdShID = "-1" }

        if (typeof seatID == "string") { holdSeID = seatID }
        else { holdSeID = "-1:-1" }

        if (typeof buyersName == "string") { holdBN = buyersName }
        else { holdBN = "unkown" }

        if (typeof email == "string") { holdE = email }
        else { holdE = "unkown"}

        if (typeof price == "number") {
            holdP = price
        } else if (typeof price == "string") {
            try {
                holdP = parseFloat(price);
            } catch (err) {
                holdP = -1;
            }
        }
        else { holdP = -1 }

        super(0, holdMID, holdShID, holdSeID, holdBN, holdE, holdP);
    }

    setTicketID(ticketID) {
        if (typeof ticketID == "string") { this.ticketID = ticketID }
        else { console.log("ticketID must be a string in setTicketID(ticketID)") }
    }

    setMovieID(movieID) {
        if (typeof movieID == "string") { this.movieID = movieID }
        else { console.log("movieID must be a string in setMovieID(movieID)") }
    }

    setShowingID(showingID) {
        if (typeof showingID == "string") { this.showingID = showingID }
        else { console.log("showingID must be a string in setShowingID(showingID)") }
    }

    setSeatID(seatID) {
        if (typeof seatID == "string") { this.seatID = seatID }
        else { console.log("seatID must be a string in setSeatID(seatID)") }
    }

    setBuyersName(buyersName) {
        if (typeof buyersName == "string") { this.buyersName = buyersName }
        else { console.log("buyersName must be a string in setBuyersName(buyersName)") }
    }

    setEmail(email) {
        if (typeof email == "string") { this.email = email }
        else { console.log("email must be a string in setEmail(email)") }
    }

    setPrice(price) {
        if (typeof price == "number") { this.price = price }
        else { console.log("price must be an number in setPrice(price)") }
    }

    getTicketID() { return this.ticketID }

    getMovieID() { return this.movieID }

    getShowingID() { return this.showingID }

    getSeatID() { return this.seatID }

    getBuyersName() { return this.buyersName }

    getEmail() { return this.email }

    getPrice() { return this.price }

    /** 
     * Gets a string representation of the instance 
     * format: movieID:showingID:seatID:buyersName:email
     */
    getStringRepresentation() {
        return `${this.movieID}:${this.showingID}:${this.seatID}:${this.buyersName}:${this.email}`;
    }
}

module.exports = Ticket;
