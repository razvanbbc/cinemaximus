/**
 * Imports -- modules, packages, assets
 */
import React from "react";
import {
    Link,
  } from "react-router-dom";

import './filmlink.css';

class FilmLinkComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            /** Pass data from specific movie query here */
            <Link className="movieLink" to={{
                pathname: "/movie",
                state: [{name: this.props.name, poster: this.props.poster}]
            }}>
            <div className="filmLinkWrapper">
                <div className="filmPosterWrapper">
                    <img src={'./' + this.props.poster} alt={this.props.name + " Poster"}/>
                </div>
                <div className="filmNameWrapper">
                    {this.props.name}
                </div>
            </div>
            </Link>
        );
    }
}

export default FilmLinkComponent;