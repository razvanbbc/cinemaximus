/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink,
  } from "react-router-dom";

import './filmdetails.css'
import BookTicketsButtonComponent from "../BookTicketsButton/BookTicketsButton";

class FilmDetailsComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="filmDetailsWrapper">
                <div className="filmPoster">
                    <img src={'./' + this.props.poster} alt={this.props.name + " Poster"}/>
                </div>
                <div className="filmROD">
                    <div className="filmName">{this.props.name}</div>
                    <div className="filmDescription">{this.props.description}</div>
                    <BookTicketsButtonComponent />
                </div>
            </div>
        );
    }

}

export default FilmDetailsComponent;
