/**
 * Imports -- modules, packages, assets
 */
import React from "react";

class BookTicketsButtonComponent extends React.Component {

    constructor(props) {
        super(props);
    }

    handleClick(e) {
        e.preventDefault();
    }

    render() {
        return (
            <a href="" onClick={this.handleClick}>
                <div className="bookTicketsButton">Book Tickets</div>
            </a>
        );
    }
}

export default BookTicketsButtonComponent;