/*
 * concrete implementation of the user class for backend
*/

/*
 * {
 *      string: userID         (primary key)
 *      string: name        (user's name)
 *      string: email       (user's email)
 *      int: phoneNo        (user's phone number)
 *      *string[]: ticketIDs   (ticket's the user has brought)
 * }
*/

const Abstract = require("../../Class Abstracts/userAbstract");

class User extends Abstract {
    constructor(name, email, phoneNo, ticketIDs = []) {
        var holdN, holdE, holdPN;

        if (typeof name == "string") { holdN = name }
        else { holdN = "unkown" }

        if (typeof email == "string") { holdE = email }
        else { holdE = "unkown" }

        if (Number.isInteger(phoneNo) == true) { holdPN = phoneNo }
        else { holdPN = -1 }

        super(0, holdN, holdE, holdPN);

        this.ticketIDs = [];
        if (typeof ticketIDs == "string") { this.ticketIDs.push(ticketIDs) }
        else if (Array.isArray(ticketIDs) == true) {
            for (var i = 0; i < ticketIDs.length; i++) {
                if (typeof ticketIDs[i] == "string") { this.ticketIDs.push(ticketIDs[i]) }
            }
        }
    }

    setUserID(userID) {
        if (typeof userID == "string") { this.userID = userID }
        else { console.log("userID must be a string in setUserID(userID)") }
    }

    setName(name) {
        if (typeof name == "string") { this.name = name }
        else { console.log("name must be a string in setName(name)") }
    }

    setEmail(email) {
        if (typeof email == "string") { this.email = email }
        else { console.log("email must be a string in setEmail(email)") }
    }

    setPhoneNo(phoneNo) {
        if (Number.isInteger(phoneNo) == true) { this.phoneNo = phoneNo }
        else { console.log("phoneNo must be a integer in setPhoneNo(phoneNo)") }
    }

    setTicketIDs(ticketIDs) {
        this.ticketIDs = [];
        if (typeof ticketIDs == "string") { this.ticketIDs.push(ticketIDs) }
        else if (Array.isArray(ticketIDs) == true) {
            for (var i = 0; i < ticketIDs.length; i++) {
                if (typeof ticketIDs[i] == "string") { this.ticketIDs.push(ticketIDs[i]) }
                else { console.log("Element " + ticketIDs[i] + " in ticketIDs is not a string") }
            }
        }
        else { console.log("ticketIDs must be a string or array in setTicketIDs(ticketIDs)") }
    }

    addTicketID(thisTicketID) {
        this.ticketIDs.push(thisTicketID);

        if (typeof thisTicketID == "string") { this.thisTicketID.push(thisTicketID[i]) }
        else if (Array.isArray(thisTicketID) == true) {
            for (var i = 0; i < thisTicketID.length; i++) {
                if (typeof thisTicketID[i] == "string") { this.thisTicketID.push(showingIDs[i]) }
                else { console.log("Element " + thisTicketID[i] + " in ticketIDs is not a string") }
            }
        }
        else { console.log("ticketIDs must be a string or array in addTicketID(thisTicketID)") } 
    }

    getUserID() { return this.userID }

    getName() { return this.name }

    getEmail() { return this.email }

    getPhoneNo() { return this.phoneNo }

    getTicketIDs() { return this.ticketIDs }
}

module.exports = User;