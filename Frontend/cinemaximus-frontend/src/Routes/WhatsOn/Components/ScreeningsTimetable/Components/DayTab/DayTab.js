/**
 * Imports -- modules, packages, assets
 */
import React from "react"; // should be imported in all components 
import {
    NavLink,
  } from "react-router-dom";

import './daytab.css';
import {getDayOfWeek} from '../../Utils/Utils';
import {getMonthOfYear} from '../../Utils/Utils';

class DayTabComponent extends React.Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        var tabDate = new Date();
        tabDate.setDate(tabDate.getDate() + this.props.id);
        const day = tabDate.getDate();
        const dayName = getDayOfWeek(tabDate);
        const monthName = getMonthOfYear(tabDate);


        this.state = {
            day: day,
            dayName: dayName.substr(0, 3),
            monthName: monthName.substr(0, 3),
        };
    }

    handleClick() {
        this.props.setActiveTab(this.props.id);
    }

    render() {


        var classes = "daytab-wrapper";
        if (this.props.active) {
            classes += " daytab-active";
        }

        return (
            <div className={classes} onClick={this.handleClick}>
                <div className="daytab-dayname">{this.state.dayName}</div>
                <div className="daytab-day">{this.state.day}</div>
                <div className="daytab-monthname">{this.state.monthName}</div>

            </div>
        );
    }

}

export default DayTabComponent;
