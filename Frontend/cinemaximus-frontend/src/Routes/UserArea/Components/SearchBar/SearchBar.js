/**
 * Imports -- modules, packages, assets
 */
import React, { setGlobal, getGlobal } from "reactn";
import './searchbar.css'

import Client from "../../../../HTTP/Client/Client";

class SearchBarComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            ticketId: ""
        }
        // let functions access the class instance
        this.search = this.search.bind(this);
        this.updateInput = this.updateInput.bind(this);
    }

    search() {
        var _this = this; // strong reference to this
        if (this.state.ticketId != "") {
            const client = new Client("http://localhost:8080");
            const data = {
                ticketID: this.state.ticketId
            }
            client.requestDataFromEndpoint("/getTicketByID", "get", data, function (response) {
                if (!response.successfulOperation) { // error occured
                    return;
                }
                setGlobal({
                    currentViewedTicket: response.data
                });
                console.log(_this.global.currentViewedTicket);
            })
        }
        else {
            // ERR HANDLING
        }
    }

    updateInput(event) {
        this.setState({ticketId: event.target.value})
    }

    render() {
        return (
            <div className="searchBarWrapper">
                <input onChange={(e) => this.updateInput(e)} name="ticket-id" className="searchInput" type="text" placeholder="Ticket ID here..." />
                <button className="searchButton" onClick={this.search}>Go</button>
            </div>
        );
    }
}

export default SearchBarComponent;