/********************* Requirements **********************/
const fetch = require("node-fetch");

//Tests the getMovieTMDB endpoint
it("Getting All Movies", async () => {
    var endpointAddress = "http://localhost:8080/getAllMovies"
    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(Object.keys(json.data).length).toBeGreaterThanOrEqual(5);
});