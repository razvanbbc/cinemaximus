/*
* Script for testing the functionality in backend
* This script runs when command 'node functionTest.js' is executed in the /Testing folder
*/

/********************* dotenv Init **********************/
require('dotenv').config(); // makes .env files available for use in the project (useful to store API keys, config vars)

/********************* NPM Requires **********************/
const express = require('express');
const cors = require('cors'); // middle package for express
const bodyParser = require('body-parser'); // for URL parameter querying
const fetch = require("node-fetch");
const url = require('url');

var firebase = require('firebase');

const response = require('express');

/********************* Implementation Requires **********************/
const ResponseClass = require('../Backend/Classes/Response/response');

const movieClass = require('../Backend/Classes/movie');
const paymentClass = require('../Backend/Classes/payment');
const showingClass = require('../Backend/Classes/showing');
const ticketClass = require('../Backend/Classes/ticket');
const userClass = require('../Backend/Classes/user');

const handleMovies = require('../Backend/Classes/Firebase/MovieSys/handleMovie');
const handlePayments = require('../Backend/Classes/Firebase/PaymentSys/handlePayment');
const handleShowings = require('../Backend/Classes/Firebase/ShowingSys/handleShowing');
const handleUsers = require('../Backend/Classes/Firebase/UserSys/handleUser');
const handleTicket = require('../Backend/Classes/Firebase/TicketSys/handleTicket');
const handleManagement = require('../Backend/Classes/Managing/handleManagement')

const TMDB = require('../Backend/TMDB');
const EmailerImport = require('../Backend/Classes/Emailer/emailer');

/********************* Firebase Config **********************/
const firebaseConfig = { // populated from .env
    apiKey: process.env.apiKey,
    authDomain: process.env.authDomain,
    databaseURL: process.env.databaseURL,
    projectId: process.env.projectId,
    storageBucket: process.env.storageBucket,
    messagingSenderId: process.env.messagingSenderId,
    appId: process.env.appId,
    measurementId: process.env.measurementId
};
try {
    firebase.initializeApp(firebaseConfig);
}
catch (e) {
    console.error(e);
}

/********************* TMDB Config **********************/
const TMDBapiKey = process.env.TMDBapiKey;

/********************* Setting up DB Handlers **********************/
const movieHandler = new handleMovies(firebase);
const paymentHandler = new handlePayments(firebase);
const showingHandler = new handleShowings(firebase);
const userHandler = new handleUsers(firebase);
const ticketHandler = new handleTicket(firebase);
const managementHandler = new handleManagement(firebase);
const Emailer = new EmailerImport(process.env.EMAILER_HOST, process.env.EMAILER_PORT, process.env.EMAILER_USER, process.env.EMAILER_PASS);

/********************* Server Listener **********************/
const port = process.env.APP_PORT; // read from .env using process.env.<var name>
const app = express();

//TestResponse class used to hold the outcome of a test, and a code inicating why
//Code -1: the test has not yet taken place
//Code 0: the test failed for an unknown reason
class TestResponse {
    constructor(id, state, code) {
        this.id = id;
        this.state = state;
        this.code = code
    }

    setState(state) { this.state = state; }
    setCode(code) { this.code = code; }

    getState() { return this.state; }
    getCode() { return this.code; }
}

app.get('/', (req, res) => {
    const noTests = 17;
    var testMovie, testPayment, testShowing, testTicket, testUser;
    var holdRes;

    var testArray = initialiseTestArray(noTests);

    console.log();
    console.log("### Testing Functionality ###");
    console.log();

    //Test 1
    holdRes = initialiseClasses();
    testArray[0].setCode(holdRes[0]);
    if (holdRes[0] == 1) { testArray[0].setState(true) }

    testMovie = holdRes[1];
    testPayment = holdRes[2];
    testShowing = holdRes[3];
    testTicket = holdRes[4];
    testUser = holdRes[5];

    //Test 2
    testTMDBByIMDBid(function (data) {
        testArray[1].setCode(data);
        if (data == 1) { testArray[1].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 3
    testTMDBByTMDBid(function (data) {
        testArray[2].setCode(data);
        if (data == 1) { testArray[2].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 4
    holdRes = testAddToDB(testMovie, testPayment, testShowing, testTicket, testUser);
    testArray[3].setCode(holdRes);
    if (holdRes == 1) { testArray[3].setState(true) }

    //Test 5
    holdRes = testGetClassByID(testMovie, testPayment, testShowing, testTicket, testUser);
    testArray[4].setCode(holdRes);
    if (holdRes == 1) { testArray[4].setState(true) }

    //Test 6
    testGetMovieByAttributes(testMovie, function (data) {
        testArray[5].setCode(data);
        if (data == 1) { testArray[5].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 7
    testGetShowingByAttributes(testShowing, function (data) {
        testArray[6].setCode(data);
        if (data == 1) { testArray[6].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 8
    testGetAllMovies(function (data) {
        testArray[7].setCode(data);
        if (data == 1) { testArray[7].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 9
    testUpdateShowings(testMovie, function (data) {
        testArray[8].setCode(data);
        if (data == 1) { testArray[8].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Tests 10
    testCalcTicketCount(function (data) {
        testArray[9].setCode(data);
        if (data == 1) { testArray[9].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 11
    testCalcTicketIncome(function (data) {
        testArray[10].setCode(data);
        if (data == 1) { testArray[10].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 12
    holdRes = testCardValidation();
    testArray[11].setCode(holdRes);
    if (holdRes == 1) { testArray[11].setState(true) }

    //Test 13
    testReserveSeat(testShowing, function (data) {
        testArray[12].setCode(data);
        if (data == 1) { testArray[12].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 14
    testSendEmail(testTicket, function (data) {
        testArray[13].setCode(data);
        if (data == 1) { testArray[13].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 15
    testGetTicket(testTicket, function (data) {
        testArray[14].setCode(data);
        if (data == 1) { testArray[14].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 16
    testResponseClass(function (data) {
        testArray[15].setCode(data);
        if (data == 1) { testArray[15].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    //Test 17
    testTicketString(testTicket, function (data) {
        testArray[16].setCode(data);
        if (data == 1) { testArray[16].setState(true) }
        checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
    });

    checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser);
});

/********************* Utility Functionality **********************/

//initialises an array of TestResponses
function initialiseTestArray(n) {
    var testArray = [];
    for (var i = 0; i < n; i++) {
        testArray.push(new TestResponse(i+1, false, -1));
    }
    return testArray;
}

//checks all tests have taken place
function checkFunctionResults(testArray, testMovie, testPayment, testShowing, testTicket, testUser) {
    var flag = true;
    var passedCount = 0;

    for (var i = 0; i < testArray.length; i++) {
        if (testArray[i].getCode() < 0) { flag = false }
        if (testArray[i].getCode() == 1) { passedCount = passedCount + 1 }
    }
    if (flag == true) {
        console.log(testArray);
        console.log("Tests passed: " + passedCount + "/" + testArray.length);
        console.log();
        cleanDBMovies(testMovie);
        cleanDBPayments(testPayment);
        cleanDBShowings(testShowing);
        cleanDBTickets(testTicket);
        cleanDBUsers(testUser);
    }
}

//cleans the DB of the test instances created
function cleanDBMovies(testMovie) {
    var ref = firebase.database().ref("Movies");
    ref.orderByChild('movieName').equalTo(testMovie.getMovieName()).on('value', (snap) => {
        ref.off('value', undefined, this);
        snap.forEach((childSnap) => {
            firebase.database().ref('Movies/' + childSnap.key).remove();
        });
    });
}

function cleanDBPayments(testPayment) {
    var ref = firebase.database().ref("Payments");
    ref.orderByChild('cardName').equalTo(testPayment.getCardName()).on('value', (snap) => {
        ref.off('value', undefined, this);
        snap.forEach((childSnap) => {
            firebase.database().ref('Payments/' + childSnap.key).remove();
        });
    });
}

function cleanDBShowings(testShowing) {
    var ref = firebase.database().ref("Showings");
    ref.orderByChild('movieID').equalTo(testShowing.getMovieID()).on('value', (snap) => {
        ref.off('value', undefined, this);
        snap.forEach((childSnap) => {
            firebase.database().ref('Showings/' + childSnap.key).remove();
        });
    });
}

function cleanDBTickets(testTicket) {
    var ref = firebase.database().ref("Tickets");
    ref.orderByChild('buyersName').equalTo(testTicket.getBuyersName()).on('value', (snap) => {
        ref.off('value', undefined, this);
        snap.forEach((childSnap) => {
            firebase.database().ref('Tickets/' + childSnap.key).remove();
        });
    });
}

function cleanDBUsers(testUser) {
    var ref = firebase.database().ref("Users");
    ref.orderByChild('name').equalTo(testUser.getName()).on('value', (snap) => {
        ref.off('value', undefined, this);
        snap.forEach((childSnap) => {
            firebase.database().ref('Users/' + childSnap.key).remove();
        });
    });
}

/********************* Testing Functionality **********************/

//Test 1
//instanciates the instances of classes to be used for testing
function initialiseClasses() {
    var resCode = []
    try {
        var testMovie = new movieClass("test movie", ["1", "2", "3", "4"], 12345);
        var testPayment = new paymentClass("test card", 1234567890, 123);
        var testShowing = new showingClass(1, Math.floor(Date.now() / 1000), 5, 5, "-1");
        var testTicket = new ticketClass("-1", "-1", "0:0", "test man", "sepcinema21@gmail.com", 10);
        var testUser = new userClass("test man", "sepcinema21@gmail.com", 01234567890, ["1", "2", "3"]);

        if (testMovie.getMovieName() == "test movie" && testPayment.getCardName() == "test card" && testShowing.getScreenNumber() == 1 && testTicket.getMovieID() == "-1" && testUser.getName() == "test man") {
            return [1, testMovie, testPayment, testShowing, testTicket, testUser]
        }
        if (testMovie.getMovieName() != "test movie") { resCode.push(2) }
        if (testPayment.getCardName() != "test card") { resCode.push(3) }
        if (testShowing.getScreenNumber() != 1) { resCode.push(4) }
        if (testTicket.getMovieID() != "-1") { resCode.push(5) }
        if (testUser.getName() != "test man") { resCode.push(6) }
        return [resCode, testMovie, testPayment, testShowing, testTicket, testUser]
    } catch {
        resCode.push(0);
        return resCode;
    }
}

//Test 2
//attempts to get the TMDB data for the movie "Soul" by its IMDB ID
function testTMDBByIMDBid(callback) {
    var soulMovie;

    try {
        TMDB.fetchByIMdB(TMDBapiKey, "tt2948372", function (response) {
            soulMovie = response;
            if (soulMovie.Title == "Soul") { callback(1) }
            else { callback(2) }
        });
    } catch { callback(0) }
}

//Test 3
//attempts to get the TMDB data for movie "Space Sweepers" by its TDMB ID
function testTMDBByTMDBid(callback) {
    var spaceSweepersMovie;

    try {
        TMDB.fetchByID(TMDBapiKey, "581389", function (response) {
            spaceSweepersMovie = response;
            if (spaceSweepersMovie.Title == "Space Sweepers") { callback(1) }
            else { callback(2) }
        });
    } catch { callback(0) }
}

//Test 4
//adds a testing instance of each class to the firebase db
function testAddToDB(testMovie, testPayment, testShowing, testTicket, testUser) {
    var resCode = []

    try {
        movieHandler.addMovie(testMovie.getMovieName(), testMovie.getShowingIDs(), testMovie.getMovieAPIID(), function (data) {
            testMovie.setMovieID(data);
        });
        paymentHandler.addPayment(testPayment.getCardName(), testPayment.getCardNo(), testPayment.getSecurityDigits(), function (data) {
            testPayment.setPaymentID(data);
        });
        showingHandler.addShowing(testShowing.getScreenNumber(), testShowing.getStartTime(), testShowing.getSeatMap(), testShowing.getMovieID(), function (data) {
            testShowing.setShowingID(data);
        });
        ticketHandler.addTicket(testTicket.getMovieID(), testTicket.getShowingID(), testTicket.getSeatID(), testTicket.getBuyersName(), testTicket.getEmail(), testTicket.getPrice(), function (data) {
            testTicket.setTicketID(data);
        });
        userHandler.addUser(testUser.getName(), testUser.getEmail(), testUser.getPhoneNo(), testUser.getTicketIDs(), function (data) {
            testUser.setUserID(data);
        });

        if (testMovie.getMovieID() != 0 && testPayment.getPaymentID() != 0 && testShowing.getShowingID() != 0 && testTicket.getTicketID() != 0 && testUser.getUserID()) {
            return 1;
        }
        if (typeof testMovie.getMovieID() != "string" || testMovie.getMovieID() == "-1") { resCode.push(2) }
        if (typeof testPayment.getPaymentID() != "string" || testPayment.getPaymentID() == "-1") { resCode.push(3) }
        if (typeof testShowing.getShowingID() != "string" || testShowing.getShowingID() == "-1") { resCode.push(4) }
        if (typeof testTicket.getTicketID() != "string" || testTicket.getTicketID() == "-1") { resCode.push(5) }
        if (typeof testUser.getUserID() != "string" || testUser.getUserID() == "-1") { resCode.push(6) }
        return resCode;
    } catch { return 0 }
}

//Test 5
// attempts to fetch the class instances added to the db in test 4 from the db
function testGetClassByID(testMovie, testPayment, testShowing, testTicket, testUser) {
    var resultMovie, resultPayment, resultShowing, resultTicket, resultUser;
    var resCode = []

    try {
        if (testMovie.getMovieID() != 0 && testPayment.getPaymentID() != 0 && testShowing.getShowingID() != 0 && testUser.getUserID()) {
            movieHandler.getMovieByID(testMovie.getMovieID(), function (data) {
                resultMovie = data;
            });
            paymentHandler.getPaymentByID(testPayment.getPaymentID(), function (data) {
                resultPayment = data;
            });
            showingHandler.getShowingByID(testShowing.getShowingID(), function (data) {
                resultShowing = data;
            });
            ticketHandler.getTicketByID(testTicket.getTicketID(), function (data) {
                resultTicket = data
            })
            userHandler.getUserByID(testUser.getUserID(), function (data) {
                resultUser = data;
            });

            if (testMovie.getMovieName() == resultMovie.getMovieName() && testPayment.getCardName() == resultPayment.getCardName() && testShowing.getStartTime() == resultShowing.getStartTime() && testTicket.getBuyersName() == resultTicket.getBuyersName() && testUser.getName() == resultUser.getName()) {
                return 1;
            }
            if (testMovie.getMovieName() != resultMovie.getMovieName()) { resCode.push(2) }
            if (testPayment.getCardName() != resultPayment.getCardName()) { resCode.push(3) }
            if (testShowing.getStartTime() != resultShowing.getStartTime()) { resCode.push(4) }
            if (testTicket.getBuyersName() != resultTicket.getBuyersName()) { resCode.push(5) }
            if (testUser.getName() != resultUser.getName()) { resCode.push(6) }
            return resCode;
        }
    } catch { return 0 }
}

//Test 6
//checks the various versions of getMovieByName etc...
function testGetMovieByAttributes(testMovie, callback) {
    var resultMovie;

    try {
        movieHandler.getMovieByName(testMovie.getMovieName(), function (data) {
            resultMovie = data;
            if (testMovie.getMovieName() == resultMovie.getMovieName()) { callback(1) }
            else { callback(2) }
        });
    } catch { callback(0) }
}

//Test 7
//checks the various versions of getShowingByTime etc...
function testGetShowingByAttributes(testShowing, callback) {
    var key, resultShowing;
    try {
        showingHandler.getShowingsByTime(testShowing.getStartTime(), testShowing.getStartTime(), function (data) {
            try {
                key = Object.keys(data)[0];
                if (data[key].startTime == testShowing.getStartTime()) { callback(1) }
                else { callback(2) }
            }
            catch { callback(2) }
        });
    } catch { callback(0) }
}

//Test 8
//checks that fetching all movies from the DB works
function testGetAllMovies(callback) {

    try {
        movieHandler.getAllMovies(function (data) {
            var keys = Object.keys(data);
            if (keys.length > 0) {
                callback(1);
            }
        });
    } catch { callback(0) }
}

//Test 9
//checks that update the list of showings in a movie instance and creating a new showing instance works
function testUpdateShowings(testMovie, callback) {
    var ref = firebase.database().ref("Movies/" + testMovie.getMovieID());
    movieHandler.updateShowings(testMovie.getMovieID(), "5", function (data) {
        if (data == true) {
            ref.get().then(function (snap) {
                var result = snap.val();
                if (result.showingIDs[4] == "5") { callback(1) }
                else { callback(3) }
            });
        } else {
            callback(2);
        }
    });
}

//Test 10
//checks that fetching ticket counts per movie works between start and end date
function testCalcTicketCount(callback) {
    var startDate = new Date(1615127900);
    var endDate = new Date(1615128010);

    managementHandler.calcTicketCount(startDate, endDate, function (counts) {
        if (counts["testManMovie1"] != 6) { callback(2) }
        else if (counts["testManMovie2"] != 2) { callback(3) }
        else { callback(1) }
    });
}

//Test 11
//checks that fetching income per movie works between start and end date
function testCalcTicketIncome(callback) {
    var startDate = new Date(1615127900);
    var endDate = new Date(1615128010);

    managementHandler.calcTicketIncome(startDate, endDate, function (incomes) {
        if (incomes["testManMovie1"] != 55) { callback(2) }
        else if (incomes["testManMovie2"] != 15) { callback(3) }
        else { callback(1) }
    });
}

//Test 12
//checks that the card validation code works
function testCardValidation() {
    var testCard1 = [[2222, 2222, 2222, 2222], 638, "11/2022"];
    var testCard2 = [[2222, 2222, 2222, 2222], 638, "13/2022"];
    var testCard3 = [[2222, 2222, 2222, 2222], 638, "11/2030"];
    var testCard4 = [[2222, 2222, 2222, 2222], 0, "11/2022"];
    var testCard5 = [[2222, 2222, 2222, 2222], 11111, "11/2022"];
    var testCard6 = [[0, 2222, 2222, 2222], 638, "11/2030"];
    var testCard7 = [[10000, 2222, 2222, 2222], 638, "11/2030"];
    var testCard8 = [[2222, 111, 2222, 2222], 638, "11/2030"];
    var testCard9 = [[2222, 2222, 111, 2222], 638, "11/2030"];
    var testCard10 = [[2222, 2222, 2222, 111], 638, "11/2030"];
    var flag = true;

    if (paymentHandler.checkCard(testCard1[0], testCard1[1], testCard1[2]) != true) { flag = false }
    if (paymentHandler.checkCard(testCard2[0], testCard2[1], testCard2[2]) != false) { flag = false }
    if (paymentHandler.checkCard(testCard3[0], testCard3[1], testCard3[2]) != false) { flag = false }
    if (paymentHandler.checkCard(testCard4[0], testCard4[1], testCard4[2]) != false) { flag = false }
    if (paymentHandler.checkCard(testCard5[0], testCard5[1], testCard5[2]) != false) { flag = false }
    if (paymentHandler.checkCard(testCard6[0], testCard6[1], testCard6[2]) != false) { flag = false }
    if (paymentHandler.checkCard(testCard7[0], testCard7[1], testCard7[2]) != false) { flag = false }
    if (paymentHandler.checkCard(testCard8[0], testCard8[1], testCard8[2]) != false) { flag = false }
    if (paymentHandler.checkCard(testCard9[0], testCard9[1], testCard9[2]) != false) { flag = false }
    if (paymentHandler.checkCard(testCard10[0], testCard10[1], testCard10[2]) != false) { flag = false }

    if (flag == true) { return 1 }
    else { return 0 }
}

//Test 13
//checks that reserving a seat for a given showing functions
function testReserveSeat(testShowing, callback) {
    try {
        ticketHandler.reserveSeat(testShowing.getShowingID(), "1:1");
        wait(1000);
        showingHandler.getShowingByID(testShowing.getShowingID(), function (data) {
            if (data.seatMap[1][1] == true) { callback(1) }
            else { callback(2) }
        });
    } catch {
        callback(0);
    }
}

//Test 14
//attempts to send an email. Must manually check if the email is recieved
function testSendEmail(testTicket, callback) {
    var testEmail = "sepcinema21@gmail.com"
    try {
        Emailer.sendEmailWithTicket({ email: testEmail, ticket: testTicket }, function (success, err) {
            if (success == true) { callback(1) }
            else {
                console.log(err);
                callback(2)
            }
        });
    } catch { callback(0) }
}

//Test 15
//checks that the ticket formatting works
function testGetTicket(testTicket, callback) {
    var res = Emailer.getHTMLTicket(testTicket);
    if (res.includes("<b> Thank you for booking a ticket at cineMaximus ! </b> <br/><hr/><b>Your Ticket</b><br/><br/>Name : test man<br/>Movie : -1<br/>Seat : 0:0<br/>Ticket ID :") && res.includes("<br/><hr/><i> - the cineMaximus team </i>&#11088;")) {
        callback(1);
    }
    else { callback(0) }
}

//Test 16
//checks that a response can be initialised, set and formatted as a JSON
function testResponseClass(callback) {
    var res = new ResponseClass(null, null, null, null, null);
    var dict = null;

    try {
        if (res.statusCode != null || res.successfulOperation != null || res.operation != null || res.message != null || res.data != null) {
            callback(2);
        }

        res.setStatus(111);
        res.setSuccessfulOperation(true);
        res.setOperations(-1);
        res.setMessage("test");
        res.setData("-111-");
        if (res.statusCode != 111 || res.successfulOperation != true || res.operation != -1 || res.message != "test" || res.data != "-111-") {
            callback(3);
        }

        dict = res.dictionary;
        if (dict.statusCode != 111 || dict.successfulOperation != true || dict.operation != -1 || dict.message != "test" || dict.data != "-111-") {
            callback(0);
        }
        else { callback(1) }
    } catch { callback(0) }
}

//Test 17
//checks the get ticket string representation
function testTicketString(testTicket, callback) {
    if (testTicket.getStringRepresentation() == "-1:-1:0:0:test man:sepcinema21@gmail.com") { callback(1) }
    else { callback(0) }
}

function wait(timeout) {
    setTimeout(function() { }, (timeout));
}

// Listen
app.listen(port, () => console.log(`Server listening on ${port} ...`));
