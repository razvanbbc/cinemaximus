/**
 * Class which acts as a middleman for requesting data from the backend
 */

 import axios from "axios"; // used for HTTP requests
 import RestResponse from "./Response/Response"; // the response format from backend

 class Client {

    /**
     * Constructor
     * @param {string} url = the first part of the url of the REST API (ie: http://localhost:8080)
     */
    constructor(url) {
        this.url = url;
    }

    /**
     * Requests data from the backend REST API
     * @param {string} endpoint = ie: "/", "getTicket, etc.
     * @param {string} method = ie: "get", "post"
     * @param {Map} parameters = ie: {firstName: 'Fred', 'lastName': 'Flintstone'}
     * 
     * Callback gives access to a RestResponse instance
     */
    requestDataFromEndpoint(endpoint, method, parameters, callback) {
        if (method == "get") {
            // GET HTTP method
            let completeURL = this.url + endpoint;
            axios.get(completeURL, {
                params: parameters
            })
            .then(function (response) { // executed after data is received
                const structuredResponse = new RestResponse( // init structured response with the data received from backend
                    response.data.statusCode, 
                    response.data.successfulOperation, 
                    response.data.operation, 
                    response.data.message, 
                    response.data.data
                    );
                callback(structuredResponse); // call a function with the structured parameter after successfully receiving data
            });
            // TODO ERR HANDLING HERE .catch()
        }
        else if (method == "post") {
            // POST HTTP method
            let completeURL = this.url + endpoint;
            axios.post(completeURL, {
                params: parameters
            })
            .then(function (response) { // executed after data is received
                const structuredResponse = new RestResponse( // init structured response with the data received from backend
                    response.data.statusCode, 
                    response.data.successfulOperation, 
                    response.data.operation, 
                    response.data.message, 
                    response.data.data
                    );
                callback(structuredResponse); // call a function with the structured parameter after successfully receiving data
            });
            // TODO ERR HANDLING HERE .catch()
        }
    }

 }

 export default Client;