/********************* Requirements **********************/
const fetch = require("node-fetch");

//Tests the getMovieTMDB endpoint
it("Getting Movies by Keywords", async () => {
    var endpointAddress = "http://localhost:8080/getMoviesByKeywords?keywords=Space,Sweepers"
    var res = await fetch(endpointAddress);
    var json = await res.json();

    expect(json.data[0].movieName).toEqual("Space Sweepers");
});