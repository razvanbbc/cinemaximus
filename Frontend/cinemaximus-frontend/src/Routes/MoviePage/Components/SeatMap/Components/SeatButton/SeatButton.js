/**
 * Imports -- modules, packages, assets
 */
import React from "react";

import './seatbutton.css';

class SeatButtonComponent extends React.Component {

    constructor(props) {
       super(props);

        this.state = {
            selected: false,
        };

       this.mouseDown = this.mouseDown.bind(this);
       this.dragSwitch = this.dragSwitch.bind(this);
       var mouseDown = 0;
    }

    mouseDown() {
        this.setState({selected: !this.state.selected});
        this.props.toggle(this.props.row, this.props.col);
        var selecting = this.state.selected ? -1 : 1;
        this.props.setSelecting(selecting);
    }

    dragSwitch() {
        var selecting = this.props.getSelecting();
        if (selecting == 0) {
            return;
        }
        if (selecting == 1) {
            if (!this.state.selected) {
                this.setState({selected: true});
                this.props.toggle(this.props.row, this.props.col);
            }
            return;
        }
        if (selecting == -1) {
            if (this.state.selected) {
                this.setState({selected: false});
                this.props.toggle(this.props.row, this.props.col);
            }
            return;
        }
    }

    render() {

        function Button(props) {
            if (!props.enabled) {
                return <div className="seat-button sb-disabled"></div>;
            }
            if (props.selected) {
                var classes = "seat-button sb-selected";
                if (props.vip) {
                    classes += " sb-vip";
                }
                return <div className={classes} onMouseEnter={props.dragSwitch} onMouseDown={props.mouseDown} ></div>;
            }
            var classes = "seat-button sb-enabled";
            if (props.vip) {
                classes += " sb-vip";
            }
            return <div className={classes} onMouseEnter={props.dragSwitch} onMouseDown={props.mouseDown} ></div>;
        }

        return (
            <div className="seat-button-wrapper">
                <Button enabled={this.props.enabled} vip={this.props.vip} selected={this.state.selected}dragSwitch={this.dragSwitch} mouseDown={this.mouseDown} />
            </div>
        );
    }

}

export default SeatButtonComponent;