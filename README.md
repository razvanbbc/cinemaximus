# CineMaximus - Abstract

Built as part of the COMP2913 (Software Engineering Project) assessment.

As per the brief, this is a cinema management system which allows users to book tickets, seats, view movie data and managers (user extensions) to analyse various business indicators for the cinema (total income over the whole cinema, income of specific movies, add movies etc).

Uses the Scrum approach, software being delivered over 3 sprints (2 weeks each) and emphasizes good developing practice, teamwork and flexibility

# CineMaximus - Tech Summary

Development -> NERF stack (NodeJS, Express, React, Firebase)\
Extras -> Postman

# Progress - Sprint 1

[x] Basic Backend  - Endpoints, simple user operations\
[x] Basic UI - Link to REST API, basic display\
[ ] Basic User System - sign-up, sign-in, user data saving\
[ ] Basic functionality - Get movie details, add movie as manager, generate tickets

# Backend (REST API)

Entry point : 'Backend/index.js'\
Built as a REST API, using NodeJS and Express to deploy the listening server.

Additional packages :
- dotenv (for config)
- Body-parser
- Cors
- Firebase (Auth, Storage)
- Shopify (later in the development process)

## API Endpoints -- Development
The backend REST API can be tested by creating a URL which matches the endpoint and navigating to it using your browser or making a request through an application like Postman.\
The port can be manually configured in the backend entry point.

1. GET [localhost:8080/] = tests whether the server is listening for requests 
2. POST [localhost:8080/signUp] with form data {email: String; pass: String} = signs up an user with the passed parameters as login credentials

## REST API Response Structure :
### Abstract :
{\
    "statusCode": Integer,\
    "successfulOperation": Boolean,\
    "operation": Integer,\
    "message": String,\
    "data": Dictionary?\
}
### Example (ie. localhost:8080/)
{\
    "statusCode": 200,\
    "successfulOperation": true,\
    "operation": 0,\
    "message": "Server is listening on port 8080",\
    "data": {"Hello": "World"}\
}

## Testing
- Simulate client and test REST API endpoints. Responses should be successful operations with data results that match the type of operation and the expected output.
- Test the implemented basic Firebase CRUD functionalities

# Frontend (Web page)

Entry point : 'Frontend/cinemaximus-frontend/src/App.js'\
Built using React (Create-react-app) and emphasizing component development and reusability. \
Will link to the backend and no other backend service. Other API calls will be handled on the backend service.

Additional packages :
- Axios
- react-to-pdf

## Testing (React TestUtils, etc.)
- Test React user events
- Test response to events above
- Test rendering of certain components at the right time (eg: user profile area is available only if user is logged in)

# How to run
0. Create a .env file in the Backend/ folder and place there the required credentials as per the code (port used for listener, Firebase credentials, API keys etc.) or contact @razvanbbc
1. Clone repository -- on master branch 
2. Make sure that you have npm installed and the required packages as per the 'package.json' file : 'npm install'
3. In the 'cinemaximus/Backend/' folder, run command 'node index.js' to start the Express app and listen for requests 
4. 'npm start' in the 'cinemaximus/Frontend/cinemaximus-frontend' folder to build and run the React app
5. Navigate to localhost:port using the addresses generated in the terminal after using the command at step 4)
