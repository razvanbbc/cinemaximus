/*
 * abstract for the payment class implementations in backend and frontend
*/

/*
 * {
 *      int: paymentID          (primary key)
 *      string: cardName        (name on the car)
 *      int: cardNo             (the card number)
 *      int: securityDigits     (security digits required to authorise a purchase)
 * }
*/

class paymentAbstract
{
    constructor(paymentID, cardName, cardNo, securityDigits)
    {
        if (this.constructor == paymentAbstract)
        {
            throw new Error("Cannot instanciate abstract class");
        }

        this.paymentID = paymentID;
        this.cardName = cardName;
        this.cardNo = cardNo;
        this.securityDigits = securityDigits;
    }

    /** Getters */
    get dictionary() // returns the format below
    { 
        return {
            "paymentID": this.paymentID,
            "cardName": this.cardName,
            "cardNo": this.cardNo,
            "securityDigits": this.securityDigits
        };
    }
}

module.exports = paymentAbstract;